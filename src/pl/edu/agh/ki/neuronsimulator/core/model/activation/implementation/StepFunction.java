package pl.edu.agh.ki.neuronsimulator.core.model.activation.implementation;

import java.math.BigDecimal;

import pl.edu.agh.ki.neuronsimulator.core.model.activation.ActivationFunction;

public class StepFunction extends ActivationFunction {
	public static final String NAME = "Step Funtion";
	private static final BigDecimal LOW_VALUE = BigDecimal.ZERO;
	private static final BigDecimal HIGH_VALUE = BigDecimal.ONE;

	public StepFunction(double factor) {
		super(factor);
	}

	public StepFunction() {
		this(1.0);
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public BigDecimal compute(BigDecimal argument) {
		return argument.compareTo(new BigDecimal(getFactor())) > 0 ? HIGH_VALUE : LOW_VALUE;
	}

	@Override
	public double compute(double argument) {
		return compute(BigDecimal.valueOf(argument)).doubleValue();
	}

	@Override
	public double computeDerivative(double argument) {
		return 0.0;
	}

	public static class Creator extends ActivationFunction.Creator {

		@Override
		public ActivationFunction newInstance(double factor) {
			return new StepFunction(factor);
		}

		@Override
		public String getName() {
			return NAME;
		}
	}

}
