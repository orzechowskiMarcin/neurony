package pl.edu.agh.ki.neuronsimulator.core.model.teaching.kohonen;

import java.util.List;

import pl.edu.agh.ki.neuronsimulator.core.exceptions.InvalidInputException;
import pl.edu.agh.ki.neuronsimulator.core.model.Coordinates;
import pl.edu.agh.ki.neuronsimulator.core.model.Layer;
import pl.edu.agh.ki.neuronsimulator.core.model.teaching.LearnigSpeedDTO;
import pl.edu.agh.ki.neuronsimulator.core.model.teaching.Teacher;
import pl.edu.agh.ki.neuronsimulator.core.model.teaching.kohonen.neighborhood.Neighborhood;
import pl.edu.agh.ki.neuronsimulator.core.model.teaching.kohonen.neighborhood.function.NeighborhoodFunction;

public class KohonenNeuralNet extends Teacher {

	private KohonenLayer kohonenLayer;
	private Layer inputLayer;

	public void setKohonenLayer(KohonenLayer kohonenLayer) {
		this.kohonenLayer = kohonenLayer;
	}

	public void setInputLayer(Layer inputLayer) {
		this.inputLayer = inputLayer;
	}

	@Override
	public void teach(double[] inputValuesTab, LearnigSpeedDTO speedsDTO) throws InvalidInputException {
		inputLayer.setValues(inputValuesTab);
		kohonenLayer.teach(new Coordinates(inputValuesTab), speedsDTO.getLearningSpeed());
	}

	@Override
	protected boolean isInputDataSizeCorrect(List<Double[]> parsedInputData) {
		return true;
		// return parsedInputData.iterator().next().length !=
		// inputLayer.getNeuronSize();
	}

	public Neighborhood getNeighborhood() {
		return kohonenLayer.getNeighborhood();
	}

	public void changeNeighborhood(Neighborhood neighborhood) {
		kohonenLayer.changeNeighborhood(neighborhood);
	}

	public void changeNeighborhoodFunction(NeighborhoodFunction function) {
		kohonenLayer.changeNeighborhoodFunction(function);
	}
}