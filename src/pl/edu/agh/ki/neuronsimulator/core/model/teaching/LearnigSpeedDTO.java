package pl.edu.agh.ki.neuronsimulator.core.model.teaching;

public class LearnigSpeedDTO {

	private double learningSpeed;
	private double momentum;

	public LearnigSpeedDTO(double learningSpeed, double momentum) {
		super();
		this.learningSpeed = learningSpeed;
		this.momentum = momentum;
	}

	/**
	 * @return the learningSpeed
	 */
	public double getLearningSpeed() {
		return learningSpeed;
	}

	/**
	 * @return the momentum or learning speed 2
	 */
	public double getMomentum() {
		return momentum;
	}

}
