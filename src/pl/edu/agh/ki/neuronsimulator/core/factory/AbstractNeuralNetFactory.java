package pl.edu.agh.ki.neuronsimulator.core.factory;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import pl.edu.agh.ki.neuronsimulator.core.exceptions.NeuralNetSchemaException;
import pl.edu.agh.ki.neuronsimulator.core.model.Layer;
import pl.edu.agh.ki.neuronsimulator.core.model.NeuralNet;
import pl.edu.agh.ki.neuronsimulator.core.model.Neuron;
import pl.edu.agh.ki.neuronsimulator.core.model.NeuronConnection;

public abstract class AbstractNeuralNetFactory {

	private final Map<NeuronDescription, NeuronParameters> neuronsDescriptions = new HashMap<>();

	private final Map<NeuronConnectionDescription, NeuronConnectionParameters> neuronConnectionsDescriptions = new HashMap<>();

	public NeuralNet buildNeuralNet() throws NeuralNetSchemaException {
		compliteNeuronDescription();

		NeuralNet neuralNet = createNeuralNetSchema();
		buildNeuronConnections(neuralNet);
		return neuralNet;
	}

	private void buildNeuronConnections(NeuralNet neuralNet) throws NeuralNetSchemaException {
		Iterator<Layer> layersIterator = neuralNet.getLayers().iterator();

		if (!layersIterator.hasNext()) {
			throw new NeuralNetSchemaException("Neural net have to has at least one layer.");
		}

		Layer previousLayer = layersIterator.next();
		while (layersIterator.hasNext()) {
			Layer currentLayer = layersIterator.next();
			connectLayers(previousLayer, currentLayer);
			previousLayer = currentLayer;
		}
	}

	private void connectLayers(Layer beginLayer, Layer endLayer) {
		for (Neuron begiNeuralNeteuron : beginLayer.getNeurons()) {
			for (Neuron endNeuron : endLayer.getNeurons()) {
				NeuronConnectionDescription connDesc = new NeuronConnectionDescription();
				connDesc.setBeginNeuronLayer(beginLayer.getLayerNumber())
						.setBeginNeuronNumber(begiNeuralNeteuron.getNumberInLayer())
						.setEndNeuronLayer(endLayer.getLayerNumber())
						.setEndNeuronNumber(endNeuron.getNumberInLayer());

				NeuronConnectionParameters coNeuralNetParam = neuronConnectionsDescriptions.get(connDesc);
				new NeuronConnection(begiNeuralNeteuron, endNeuron, coNeuralNetParam.getWeight());
			}
		}
	}

	private void compliteNeuronDescription() {
		for (NeuronConnectionDescription desc : neuronConnectionsDescriptions.keySet()) {
			addNeuronIfNotPresent(desc.getBeginNeuron());
			addNeuronIfNotPresent(desc.getEndNeuron());
		}
	}

	private void addNeuronIfNotPresent(NeuronDescription neuronDesc) {
		if (!neuronsDescriptions.containsKey(neuronDesc)) {
			addNeuronDescription(neuronDesc, new NeuronParameters());
		}
	}

	private NeuralNet createNeuralNetSchema() throws NeuralNetSchemaException {
		if (neuronsDescriptions.isEmpty()) {
			throw new NeuralNetSchemaException("Neural net have to has at least one layer.");
		}

		Map<Integer, Layer> layers = new TreeMap<>();
		for (Map.Entry<NeuronDescription, NeuronParameters> entry : neuronsDescriptions.entrySet()) {
			Layer currentLayer;
			if (!layers.containsKey(entry.getKey().getNeuronLayer())) {
				currentLayer = newLayer(entry.getKey().getNeuronLayer());
				layers.put(entry.getKey().getNeuronLayer(), currentLayer);
			} else {
				currentLayer = layers.get(entry.getKey().getNeuronLayer());
			}

			Neuron neuron = newNeuron(currentLayer, entry.getKey().getNeuronNumber(), entry.getValue().getBias());
			currentLayer.addNeuron(neuron);
		}

		// TODO sprwadzeni ciaglosci kolejnych warstw
		return newNeuralNet(layers);
	}

	public void addConnectionDescription(NeuronConnectionDescription coNeuralNetDesc, NeuronConnectionParameters coNeuralNetParam) {
		neuronConnectionsDescriptions.put(coNeuralNetDesc, coNeuralNetParam);
	}

	public void addNeuronDescription(NeuronDescription neuronDesc, NeuronParameters neuronParam) {
		neuronsDescriptions.put(neuronDesc, neuronParam);
	}

	protected abstract Layer newLayer(int layerNumber);

	protected abstract NeuralNet newNeuralNet(Map<Integer, Layer> layers);

	protected abstract Neuron newNeuron(Layer layer, int numberInLayer, double bias);
}
