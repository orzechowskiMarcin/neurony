package pl.edu.agh.ki.neuronsimulator.gui.views;

import java.awt.Frame;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JApplet;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.awt.SWT_AWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;

import pl.edu.agh.ki.neuronsimulator.core.model.NeuralNet;
import pl.edu.agh.ki.neuronsimulator.core.model.Neuron;
import pl.edu.agh.ki.neuronsimulator.core.model.NeuronConnection;
import pl.edu.agh.ki.neuronsimulator.gui.Activator;
import pl.edu.agh.ki.neuronsimulator.gui.graph.NeuralNetWrapper;
import edu.uci.ics.jung.algorithms.layout.FRLayout;
import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.control.DefaultModalGraphMouse;
import edu.uci.ics.jung.visualization.control.ModalGraphMouse;
import edu.uci.ics.jung.visualization.decorators.ToStringLabeller;

public class NeuralNetVisualistaionView extends NeuronViewPart {
	public static final String ID = "pl.edu.agh.ki.neuronsimulator.gui.views.NeuralNetVisualistaionView";

	private Composite composite;
	private JApplet applet;
	private IPropertyChangeListener eventListener;
	private Frame frame;
	private VisualizationViewer<Neuron, NeuronConnection> visualizationViewer;

	public NeuralNetVisualistaionView() {
		super();
		eventListener = new IPropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent event) {
				if (event.getProperty().equals(Activator.NEURAL_NET_LOADED)) {
					Object argument = event.getNewValue();
					if (argument instanceof NeuralNet) {
						createNeuronNet((NeuralNet) argument);
					} else {
						// TODO obsluzyc - pokazac komunikat
					}
				} else if (event.getProperty().equals(Activator.RELOAD_NEURAL_NET_VISUALISATION)) {
					repaintGraph();
				}
			}
		};
	}

	@Override
	public void createPartControl(Composite parent) {
		Activator.getDefault().getPreferenceStore().addPropertyChangeListener(eventListener);

		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
			// cannot set system "look and feel" - ignore
		}

		composite = new Composite(parent, SWT.EMBEDDED | SWT.NO_BACKGROUND);
		frame = SWT_AWT.new_Frame(composite);
		applet = new JApplet();
		frame.add(applet);

		applet.setVisible(true);
		// frame.pack();
	}

	@Override
	public void setFocus() {
		composite.setFocus();
	}

	private void repaintGraph() {
		visualizationViewer.repaint();
	}

	private void createNeuronNet(final NeuralNet neuralNet) {
		ProgressMonitorDialog dialog = new ProgressMonitorDialog(getSite().getShell());
		try {
			dialog.run(true, true, new IRunnableWithProgress() {

				@Override
				public void run(IProgressMonitor monitor) throws InvocationTargetException, InterruptedException {
					monitor.beginTask("Creating neural net visualisation", 3);
					monitor.subTask("Computing visualsations properties");

					NeuralNetWrapper netWrapper = new NeuralNetWrapper(neuralNet, applet.getSize());

					Layout<Neuron, NeuronConnection> layout = new FRLayout<>(netWrapper.getGraph());
					layout.setSize(applet.getSize());
					for (Neuron neuron : netWrapper.getGraph().getVertices()) {
						layout.setLocation(neuron, neuron.getLocation());
						layout.lock(neuron, true);
					}

					monitor.worked(1);
					monitor.subTask("Setting view properties");

					visualizationViewer = new VisualizationViewer<>(layout);
					visualizationViewer.setPreferredSize(applet.getSize());

					visualizationViewer.getRenderContext().setEdgeLabelTransformer(new ToStringLabeller<NeuronConnection>());
					visualizationViewer.getRenderContext().setVertexLabelTransformer(new ToStringLabeller<Neuron>());

					DefaultModalGraphMouse<Neuron, NeuronConnection> graphMouse = new DefaultModalGraphMouse<Neuron, NeuronConnection>();
					graphMouse.setMode(ModalGraphMouse.Mode.PICKING);
					visualizationViewer.setGraphMouse(graphMouse);

					visualizationViewer.getPickedVertexState().addItemListener(new ItemListener() {
						@Override
						public void itemStateChanged(ItemEvent e) {
							fireEvent(Activator.VERTEX_PICKED, e.getItem());
						}
					});

					visualizationViewer.getPickedEdgeState().addItemListener(new ItemListener() {
						@Override
						public void itemStateChanged(ItemEvent e) {
							fireEvent(Activator.EDGE_PICKED, e.getItem());
						}
					});

					monitor.worked(1);
					monitor.subTask("Adding visualization");

					Display.getDefault().syncExec(new Runnable() {

						@Override
						public void run() {
							applet.getContentPane().removeAll();
							applet.getContentPane().add(visualizationViewer);
							frame.pack();
							repaintGraph();
						}
					});

					monitor.worked(1);
					monitor.done();
				}
			});
		} catch (InvocationTargetException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void fireEvent(final String eventName, final Object sourceObject) {
		Display.getDefault().asyncExec(new Runnable() {

			@Override
			public void run() {
				Activator.getDefault().getPreferenceStore().firePropertyChangeEvent(eventName, new Object(), sourceObject);
			}
		});
	}
}