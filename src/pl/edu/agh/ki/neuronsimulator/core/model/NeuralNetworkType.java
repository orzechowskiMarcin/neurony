package pl.edu.agh.ki.neuronsimulator.core.model;

public enum NeuralNetworkType {

	FEED_FORWARD, KOHONEN
}
