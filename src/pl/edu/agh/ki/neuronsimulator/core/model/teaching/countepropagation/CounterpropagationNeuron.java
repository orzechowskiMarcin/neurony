package pl.edu.agh.ki.neuronsimulator.core.model.teaching.countepropagation;

import pl.edu.agh.ki.neuronsimulator.core.model.Neuron;
import pl.edu.agh.ki.neuronsimulator.core.model.NeuronConnection;

public class CounterpropagationNeuron {

	private Neuron neuron;

	public CounterpropagationNeuron(Neuron neuron) {
		this.neuron = neuron;
	}

	public Neuron getNeuron() {
		return neuron;
	}

	public void teach(double expectedOutputValue, double learningSpeed) {
		for (NeuronConnection conn : neuron.getInConnections()) {
			teachConnection(conn, expectedOutputValue, learningSpeed);
		}
	}

	private void teachConnection(NeuronConnection conn, double expectedOutputValue, double learningSpeed) {
		double currentWeight = conn.getWeight();
		double currentOutput = neuron.getValueAsDouble();
		double signalFromKohonenNeuron = conn.getBeginNeuron().getValueAsDouble();

		double newWeight = currentWeight + learningSpeed * (expectedOutputValue - currentOutput) * signalFromKohonenNeuron;

		conn.setWegiht(newWeight);
	}
}
