package pl.edu.agh.ki.neuronsimulator.core.model.teaching.backpropagation.algorithm;

import pl.edu.agh.ki.neuronsimulator.core.model.activation.ActivationFunction;

public class BackpropagationsParams {

	private boolean isHiden;

	/**
	 * n1
	 */
	private double learningSpeedFactor;
	/**
	 * tj
	 */
	private double expectingOutputSignal;
	/**
	 * ej - suma wszystkich wejsc(wartosc wejscia * waga polaczenia)
	 */
	private double inputSum;
	/**
	 * wlj - wagi wszystkich polaczen wychodzacych * blad neuronu na tej wadze
	 */
	private double outputWeights;
	/**
	 * xj
	 */
	private double inputSignal;
	/**
	 * f
	 */
	private ActivationFunction activationFunction;
	/**
	 * n2
	 */
	private double momentum;
	/**
	 * mji
	 */
	private double previousWeightChange;

	private BackpropagationsParams(boolean isHiden) {
		this.isHiden = isHiden;
	}

	public static BackpropagationsParams build(boolean isHiden) {
		return new BackpropagationsParams(isHiden);
	}

	private BackpropagationsParams copy() {
		BackpropagationsParams copiedParams = new BackpropagationsParams(this.isHiden);
		copiedParams.learningSpeedFactor = this.learningSpeedFactor;
		copiedParams.expectingOutputSignal = this.expectingOutputSignal;
		copiedParams.inputSum = this.inputSum;
		copiedParams.outputWeights = this.outputWeights;
		copiedParams.inputSignal = this.inputSignal;
		copiedParams.activationFunction = this.activationFunction;
		copiedParams.momentum = this.momentum;
		copiedParams.previousWeightChange = this.previousWeightChange;
		return copiedParams;
	}

	/**
	 * n1
	 */
	public double getLearningSpeedFactor() {
		return learningSpeedFactor;
	}

	/**
	 * n1
	 */
	public BackpropagationsParams withLearningSpeedFactor(double learningSpeedFactor) {
		BackpropagationsParams copiedParams = copy();
		copiedParams.learningSpeedFactor = learningSpeedFactor;
		return copiedParams;
	}

	/**
	 * tj
	 */
	public double getExpectingOutputSignal() {
		return expectingOutputSignal;
	}

	/**
	 * tj
	 */
	public BackpropagationsParams withExpectingOutputSignal(double expectingOutputSignal) {
		BackpropagationsParams copiedParams = copy();
		copiedParams.expectingOutputSignal = expectingOutputSignal;
		return copiedParams;
	}

	/**
	 * ej - suma wszystkich wejsc(wartosc wejscia * waga polaczenia)
	 */
	public double getInputSum() {
		return inputSum;
	}

	/**
	 * ej - suma wszystkich wejsc(wartosc wejscia * waga polaczenia)
	 */
	public BackpropagationsParams withInputSum(double inputSum) {
		BackpropagationsParams copiedParams = copy();
		copiedParams.inputSum = inputSum;
		return copiedParams;
	}

	/**
	 * wlj - wagi wszystkich polaczen wychodzacych * blad neuronu na tej wadze
	 */
	public double getOutputWeights() {
		return outputWeights;
	}

	/**
	 * wlj - wagi wszystkich polaczen wychodzacych * blad neuronu na tej wadze
	 */
	public BackpropagationsParams withOutputWeightsWithFault(double outputWeights) {
		BackpropagationsParams copiedParams = copy();
		copiedParams.outputWeights = outputWeights;
		return copiedParams;
	}

	/**
	 * xj
	 */
	public double getInputSignal() {
		return inputSignal;
	}

	/**
	 * xj
	 */
	public BackpropagationsParams withInputSignal(double inputSignal) {
		BackpropagationsParams copiedParams = copy();
		copiedParams.inputSignal = inputSignal;
		return copiedParams;
	}

	/**
	 * f
	 */
	public ActivationFunction getActivationFunction() {
		return activationFunction;
	}

	/**
	 * f
	 */
	public BackpropagationsParams withActivationFunction(ActivationFunction activationFunction) {
		BackpropagationsParams copiedParams = copy();
		copiedParams.activationFunction = activationFunction;
		return copiedParams;
	}

	/**
	 * n2
	 */
	public double getMomentum() {
		return momentum;
	}

	/**
	 * n2
	 */
	public BackpropagationsParams withMomentum(double momentum) {
		BackpropagationsParams copiedParams = copy();
		copiedParams.momentum = momentum;
		return copiedParams;
	}

	/**
	 * mji
	 */
	public double getPreviousWeightChange() {
		return previousWeightChange;
	}

	/**
	 * mji
	 */
	public BackpropagationsParams withPreviousWeightChange(double previousWeightChange) {
		BackpropagationsParams copiedParams = copy();
		copiedParams.previousWeightChange = previousWeightChange;
		return copiedParams;
	}

	/**
	 * @return the isHiden
	 */
	public boolean isHiden() {
		return isHiden;
	}
}
