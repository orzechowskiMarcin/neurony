package pl.edu.agh.ki.neuronsimulator.core.model.activation.implementation;

import java.math.BigDecimal;

import pl.edu.agh.ki.neuronsimulator.core.model.activation.ActivationFunction;

public class SigmoidalFuntion extends ActivationFunction {
	public static final String NAME = "Sigmoidal Funtion";

	public SigmoidalFuntion(double factor) {
		super(factor);
	}

	public SigmoidalFuntion() {
		this(1.0);
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public BigDecimal compute(BigDecimal argument) {
		// TODO zepsute - utrata precyzji!!!

		BigDecimal argForExp = argument.multiply(new BigDecimal(getFactor())).negate();
		double result = 1.0 / (1.0 + Math.exp(argForExp.doubleValue()));
		return new BigDecimal(result);
	}

	@Override
	public double compute(double argument) {
		return compute(BigDecimal.valueOf(argument)).doubleValue();
	}

	@Override
	public double computeDerivative(double argument) {
		double normalFunctionVal = compute(argument);
		return normalFunctionVal * (1.0 - normalFunctionVal);
	}

	public static class Creator extends ActivationFunction.Creator {

		@Override
		public ActivationFunction newInstance(double factor) {
			return new SigmoidalFuntion(factor);
		}

		@Override
		public String getName() {
			return NAME;
		}
	}

}
