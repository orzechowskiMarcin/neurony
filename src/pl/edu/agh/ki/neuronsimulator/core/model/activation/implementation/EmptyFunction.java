package pl.edu.agh.ki.neuronsimulator.core.model.activation.implementation;

import java.math.BigDecimal;

import pl.edu.agh.ki.neuronsimulator.core.model.activation.ActivationFunction;

public class EmptyFunction extends ActivationFunction {

	private static final String NAME = "Empty Function";

	public EmptyFunction(double factor) {
		super(factor);
	}

	@Override
	public BigDecimal compute(BigDecimal argument) {
		return argument;
	}

	@Override
	public double compute(double argument) {
		return argument;
	}

	@Override
	public double computeDerivative(double argument) {
		throw new RuntimeException("Method should not be used!");
	}

	@Override
	public String getName() {
		return NAME;
	}

	public static class Creator extends ActivationFunction.Creator {

		@Override
		public String getName() {
			return NAME;
		}

		@Override
		public ActivationFunction newInstance(double factor) {
			return new EmptyFunction(factor);
		}
	}
}
