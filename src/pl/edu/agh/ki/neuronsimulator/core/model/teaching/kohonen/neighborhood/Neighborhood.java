package pl.edu.agh.ki.neuronsimulator.core.model.teaching.kohonen.neighborhood;

import java.util.Collection;

import pl.edu.agh.ki.neuronsimulator.core.model.teaching.kohonen.KohonenNeuron;

public abstract class Neighborhood {

	public abstract void buildNeighborhood(Collection<KohonenNeuron> neurons);

	public abstract int getRadius(KohonenNeuron fromNeuron, KohonenNeuron toNeuron);
}
