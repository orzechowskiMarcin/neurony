package pl.edu.agh.ki.neuronsimulator.core.factory;

public class NeuronDescription {

	private int neuronLayer;
	private int neuronNumber;

	public int getNeuronLayer() {
		return neuronLayer;
	}

	public NeuronDescription setNeuronLayer(int neuronLayer) {
		validatePositiveNumber(neuronLayer);
		this.neuronLayer = neuronLayer;
		return this;
	}

	public NeuronDescription setNeuronLayer(String neuronLayer) {
		return setNeuronLayer(Integer.parseInt(neuronLayer));
	}

	public int getNeuronNumber() {
		return neuronNumber;
	}

	public NeuronDescription setNeuronNumber(int neuronNumber) {
		validatePositiveNumber(neuronNumber);
		this.neuronNumber = neuronNumber;
		return this;
	}

	public NeuronDescription setNeuronNumber(String neuronNumber) {
		return setNeuronNumber(Integer.parseInt(neuronNumber));
	}

	private void validatePositiveNumber(int number) {
		if (number < 0) {
			throw new IllegalArgumentException("Layer number and neuron number have to be positive.");
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof NeuronDescription)) {
			return false;
		}

		NeuronDescription other = (NeuronDescription) obj;
		return this.neuronLayer == other.neuronLayer && this.neuronNumber == other.neuronNumber;
	}

	@Override
	public int hashCode() {
		int hashCode = Integer.valueOf(neuronLayer).hashCode();
		hashCode = hashCode / 2 + Integer.valueOf(neuronNumber).hashCode() / 2;
		return hashCode;
	}
}
