package pl.edu.agh.ki.neuronsimulator.core.model.teaching.kohonen.neighborhood.function;

public class FunctionWithDisabledNeigborhood implements NeighborhoodFunction {

	@Override
	public double compute(int radius) {
		return radius == 0 ? 1 : 0;
	}

}
