package pl.edu.agh.ki.neuronsimulator.gui.handlers;

import pl.edu.agh.ki.neuronsimulator.core.factory.BackpropagationNeuronFactory;
import pl.edu.agh.ki.neuronsimulator.core.model.NeuralNet;
import pl.edu.agh.ki.neuronsimulator.core.model.teaching.backpropagation.BackpropagationNet;
import pl.edu.agh.ki.neuronsimulator.gui.Activator;

public class BackpropagationNeuralNetLoadHandler extends AbstractNeuralNetLoadHandler {

	@Override
	protected void onNeuralNetBroadCasted(NeuralNet neuralNet) {
		BackpropagationNeuronFactory factory = new BackpropagationNeuronFactory();
		BackpropagationNet backpropagationNet = factory.build(neuralNet);
		Activator
				.getDefault()
				.getPreferenceStore()
				.firePropertyChangeEvent(Activator.BACKPROPAGATION_TEACHER_LOADED, new Object(), backpropagationNet);
	}

}
