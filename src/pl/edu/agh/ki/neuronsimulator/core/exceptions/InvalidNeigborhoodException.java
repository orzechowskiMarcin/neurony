package pl.edu.agh.ki.neuronsimulator.core.exceptions;

public class InvalidNeigborhoodException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public InvalidNeigborhoodException(String msg) {
		super(msg);
	}
}
