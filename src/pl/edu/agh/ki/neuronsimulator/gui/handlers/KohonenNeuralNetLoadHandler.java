package pl.edu.agh.ki.neuronsimulator.gui.handlers;

import pl.edu.agh.ki.neuronsimulator.core.exceptions.NeuralNetSchemaException;
import pl.edu.agh.ki.neuronsimulator.core.factory.KohonenNeuralNetFactory;
import pl.edu.agh.ki.neuronsimulator.core.model.NeuralNet;
import pl.edu.agh.ki.neuronsimulator.core.model.teaching.kohonen.KohonenNeuralNet;
import pl.edu.agh.ki.neuronsimulator.gui.Activator;

public class KohonenNeuralNetLoadHandler extends AbstractNeuralNetLoadHandler {

	@Override
	protected void onNeuralNetBroadCasted(NeuralNet neuralNet) throws NeuralNetSchemaException {
		KohonenNeuralNetFactory factory = new KohonenNeuralNetFactory();
		KohonenNeuralNet kohonenNeuralNet = factory.build(neuralNet);
		Activator
				.getDefault()
				.getPreferenceStore()
				.firePropertyChangeEvent(Activator.KOHONENE_TEACHER_LOADED, new Object(), kohonenNeuralNet);
	}

}
