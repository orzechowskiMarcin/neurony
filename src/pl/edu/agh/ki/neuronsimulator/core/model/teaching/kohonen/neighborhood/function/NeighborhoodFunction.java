package pl.edu.agh.ki.neuronsimulator.core.model.teaching.kohonen.neighborhood.function;

public interface NeighborhoodFunction {

	double compute(int radius);
}
