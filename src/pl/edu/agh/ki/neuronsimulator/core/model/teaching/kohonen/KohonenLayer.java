package pl.edu.agh.ki.neuronsimulator.core.model.teaching.kohonen;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import pl.edu.agh.ki.neuronsimulator.core.model.Coordinates;
import pl.edu.agh.ki.neuronsimulator.core.model.Layer;
import pl.edu.agh.ki.neuronsimulator.core.model.Neuron;
import pl.edu.agh.ki.neuronsimulator.core.model.teaching.kohonen.neighborhood.Neighborhood;
import pl.edu.agh.ki.neuronsimulator.core.model.teaching.kohonen.neighborhood.function.NeighborhoodFunction;

public class KohonenLayer {

	private List<KohonenNeuron> neurons = new ArrayList<>();
	// remember to initialize
	private Neighborhood neighborhood;
	private NeighborhoodFunction function;

	public KohonenLayer(Layer layer) {
		for (Neuron neuron : layer.getNeurons()) {
			neurons.add(new KohonenNeuron(neuron));
		}
	}

	public void teach(Coordinates inputCoordinates, double learningSpeed) {
		KohonenNeuron winnerNeuron = findWinnerNeuron(inputCoordinates);

		for (KohonenNeuron neuron : neurons) {
			int radius = neighborhood.getRadius(winnerNeuron, neuron);
			double neigborhoodModification = function.compute(radius);
			neuron.teach(inputCoordinates, neigborhoodModification, learningSpeed);
		}
	}

	private KohonenNeuron findWinnerNeuron(Coordinates inputCoordinates) {
		Iterator<KohonenNeuron> neuronIt = neurons.iterator();

		KohonenNeuron closestNeuron = neuronIt.next();
		double closestNeuronDistance = closestNeuron.calculateEuclidianDisctance(inputCoordinates);

		while (neuronIt.hasNext()) {
			KohonenNeuron currentNeuron = neuronIt.next();
			double currentNeuronDisctance = currentNeuron.calculateEuclidianDisctance(inputCoordinates);

			if (currentNeuronDisctance < closestNeuronDistance) {
				closestNeuronDistance = currentNeuronDisctance;
				closestNeuron = currentNeuron;
			}
		}

		return closestNeuron;
	}

	public void changeNeighborhood(Neighborhood neighborhood) {
		this.neighborhood = neighborhood;
		this.neighborhood.buildNeighborhood(neurons);
	}

	public Neighborhood getNeighborhood() {
		return neighborhood;
	}

	public void changeNeighborhoodFunction(NeighborhoodFunction function) {
		this.function = function;
	}

}
