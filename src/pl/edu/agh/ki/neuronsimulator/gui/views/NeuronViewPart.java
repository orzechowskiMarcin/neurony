package pl.edu.agh.ki.neuronsimulator.gui.views;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;

public abstract class NeuronViewPart extends ViewPart {

	public NeuronViewPart() {
		super();
	}

	protected void reportError(String title, String msg, Throwable t) {
		Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
		MessageDialog.openError(shell, title, msg);
		t.printStackTrace();
	}

}