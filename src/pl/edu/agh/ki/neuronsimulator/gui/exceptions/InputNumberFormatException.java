package pl.edu.agh.ki.neuronsimulator.gui.exceptions;

import java.text.MessageFormat;

public class InputNumberFormatException extends Exception {

	private static final long serialVersionUID = 1L;
	private static final String MSG = "\"{0}\" must be a correct number.";
	private final String attributeName;

	public InputNumberFormatException(String attributeName, NumberFormatException e) {
		super(e);
		this.attributeName = attributeName;
	}

	@Override
	public String getMessage() {
		return MessageFormat.format(MSG, attributeName);
	}
}
