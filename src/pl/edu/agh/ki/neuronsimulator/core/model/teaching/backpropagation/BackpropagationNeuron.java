package pl.edu.agh.ki.neuronsimulator.core.model.teaching.backpropagation;

import java.util.HashMap;
import java.util.Map;

import pl.edu.agh.ki.neuronsimulator.core.model.Neuron;
import pl.edu.agh.ki.neuronsimulator.core.model.NeuronConnection;
import pl.edu.agh.ki.neuronsimulator.core.model.teaching.backpropagation.algorithm.Backpropagations;
import pl.edu.agh.ki.neuronsimulator.core.model.teaching.backpropagation.algorithm.BackpropagationsParams;

public class BackpropagationNeuron {

	private Map<NeuronConnection, BackpropagationConnection> connectionsMapping = new HashMap<>();
	private Neuron neuron;
	private double neuronFault = 0.0;
	private double previousBiasChange = 0.0;

	public BackpropagationNeuron(Neuron neuron) {
		this.neuron = neuron;
	}

	public void addConnection(BackpropagationConnection connection) {
		connectionsMapping.put(connection.getNeuronConnection(), connection);
	}

	public Neuron getNeuron() {
		return neuron;
	}

	public double getNeuronFault() {
		return neuronFault;
	}

	public void teach(BackpropagationsParams params) {
		BackpropagationsParams newParams = params.withInputSum(neuron.getAllInputsSum().doubleValue());

		double faultFromNextLayer = 0.0;
		for (NeuronConnection conn : neuron.getOutConnections()) {
			faultFromNextLayer += countFaultFromNextLayer(connectionsMapping.get(conn));
		}
		newParams = newParams.withOutputWeightsWithFault(faultFromNextLayer);

		for (NeuronConnection conn : neuron.getInConnections()) {
			connectionsMapping.get(conn).teach(newParams);
		}

		computeBiasChange(newParams);

		neuronFault = Backpropagations.computeFault(newParams);
	}

	private void computeBiasChange(BackpropagationsParams params) {
		BackpropagationsParams newParams = params.withInputSignal(1.0).withPreviousWeightChange(previousBiasChange);

		double biasChange = Backpropagations.computeWeightChange(newParams);
		double newBias = neuron.getBias() + biasChange;
		neuron.setBiasAsDouble(newBias);
		previousBiasChange = biasChange;
	}

	private double countFaultFromNextLayer(BackpropagationConnection backpropagationConnection) {
		return backpropagationConnection.getFaultMultipledByWeight();
	}
}
