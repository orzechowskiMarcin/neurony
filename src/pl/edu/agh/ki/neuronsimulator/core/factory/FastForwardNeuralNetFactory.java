package pl.edu.agh.ki.neuronsimulator.core.factory;

import java.util.Map;

import pl.edu.agh.ki.neuronsimulator.core.model.Layer;
import pl.edu.agh.ki.neuronsimulator.core.model.NeuralNet;
import pl.edu.agh.ki.neuronsimulator.core.model.Neuron;

public class FastForwardNeuralNetFactory extends AbstractNeuralNetFactory {

	@Override
	protected Layer newLayer(int layerNumber) {
		return new Layer(layerNumber);
	}

	@Override
	protected NeuralNet newNeuralNet(Map<Integer, Layer> layers) {
		return new NeuralNet(layers);
	}

	@Override
	protected Neuron newNeuron(Layer layer, int numberInLayer, double bias) {
		return new Neuron(layer, numberInLayer, bias);
	}

}
