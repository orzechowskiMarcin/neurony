package pl.edu.agh.ki.neuronsimulator.core.exceptions;

public class CannotTeachNetException extends Exception {

	private static final long serialVersionUID = 1L;

	public CannotTeachNetException(String msg) {
		super(msg);
	}

	public CannotTeachNetException(String msg, Throwable e) {
		super(msg, e);
	}

	public CannotTeachNetException(InvalidInputException e) {
		super(e);
	}
}
