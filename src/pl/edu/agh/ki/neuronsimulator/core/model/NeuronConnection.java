package pl.edu.agh.ki.neuronsimulator.core.model;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.UUID;

public class NeuronConnection {
	private DecimalFormat formatter = new DecimalFormat("#.####");

	private final Neuron beginNeuron;
	private final Neuron endNeuron;

	private double weight;

	public NeuronConnection(Neuron beginNeuron, Neuron endNeuron, double weight) {
		this.beginNeuron = beginNeuron;
		this.endNeuron = endNeuron;

		this.beginNeuron.addOutConnection(this);
		this.endNeuron.addInConnection(this);

		this.weight = weight;
	}

	public Neuron getBeginNeuron() {
		return beginNeuron;
	}

	public Neuron getEndNeuron() {
		return endNeuron;
	}

	public double getWeight() {
		return weight;
	}

	public void setWegiht(double weight) {
		this.weight = weight;
	}

	@Override
	public String toString() {
		return formatter.format(weight);
	}

	public String getName() {
		StringBuilder builder = beginNeuron.getNameBuilder();
		builder.append(" -> ");
		builder.append(endNeuron.getNameBuilder());
		return builder.toString();
	}

	protected BigDecimal computeValue(UUID currentComputationUUID) {
		return new BigDecimal(weight).multiply(beginNeuron.compute(currentComputationUUID));
	}

	public int getInputDimmensionNumber() {
		return beginNeuron.getNumberInLayer();
	}

	public double getWeightAsDouble() {
		return weight;
	}

	public String getWeightAsString() {
		return Double.toString(weight);
	}

	@Override
	public int hashCode() {
		// FIXME do dupy
		return beginNeuron.hashCode() + endNeuron.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof NeuronConnection)) {
			return false;
		}
		NeuronConnection other = (NeuronConnection) obj;
		return other.getBeginNeuron().equals(this.getBeginNeuron()) && other.getEndNeuron().equals(this.getEndNeuron());
	}

}