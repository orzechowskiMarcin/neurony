package pl.edu.agh.ki.neuronsimulator.gui.graph;

import java.awt.Dimension;
import java.util.List;

import pl.edu.agh.ki.neuronsimulator.core.model.Layer;
import pl.edu.agh.ki.neuronsimulator.core.model.NeuralNet;
import pl.edu.agh.ki.neuronsimulator.core.model.Neuron;
import pl.edu.agh.ki.neuronsimulator.core.model.NeuronConnection;
import edu.uci.ics.jung.graph.DirectedGraph;
import edu.uci.ics.jung.graph.DirectedSparseGraph;

public class NeuralNetWrapper {
	private static final int MARGIN = 40;

	private DirectedGraph<Neuron, NeuronConnection> graph = new DirectedSparseGraph<>();

	public NeuralNetWrapper(NeuralNet net, Dimension dimension) {
		computeNeuronsLocations(net, dimension);
		buildGraph(net);
	}

	private void computeNeuronsLocations(NeuralNet net, Dimension dimension) {
		double height = dimension.height - 2.0 * MARGIN;
		double width = dimension.width - 2.0 * MARGIN;
		double spaceBeetwenLayers = height / net.getLayersSize();
		double spaceBeetwenNeurons = width / net.getNuronsSize();

		int maxNeuronSize = net.getNuronsSize();
		for (Layer layer : net.getLayers()) {
			double verticalPosition = transform(height, layer.getLayerNumber() * spaceBeetwenLayers);
			double horizontalTransformationInLayer = (maxNeuronSize - layer.getNeuronSize()) / 2.0 * spaceBeetwenNeurons;
			for (Neuron neuron : layer.getNeurons()) {
				double horizontalPosition = horizontalTransformationInLayer + neuron.getNumberInLayer() * spaceBeetwenNeurons + MARGIN;
				neuron.setHorizontalPosition(horizontalPosition);
				neuron.setVerticalPosition(verticalPosition);
			}
		}
	}

	private double transform(double dimension, double computedLocation) {
		return dimension - computedLocation + MARGIN;
	}

	private void buildGraph(NeuralNet net) {
		createVerticesFromNeurons(net.getAllNeurons());
		createEdgesFromNeuronConnections(net.getAllNeuronConnections());
	}

	private void createEdgesFromNeuronConnections(List<NeuronConnection> neuronConnections) {
		for (NeuronConnection connection : neuronConnections) {
			graph.addEdge(connection, connection.getBeginNeuron(), connection.getEndNeuron());
		}
	}

	private void createVerticesFromNeurons(List<Neuron> neurons) {
		for (Neuron neuron : neurons) {
			graph.addVertex(neuron);
		}
	}

	public DirectedGraph<Neuron, NeuronConnection> getGraph() {
		return graph;
	}
}
