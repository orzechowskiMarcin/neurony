package pl.edu.agh.ki.neuronsimulator.core.model.teaching.kohonen.neighborhood.function;

public class FunctionWithDisableCompetitionAndNeigborhood implements NeighborhoodFunction {

	@Override
	public double compute(int radius) {
		return 1;
	}

}
