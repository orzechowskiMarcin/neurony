package pl.edu.agh.ki.neuronsimulator.core.factory;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import pl.edu.agh.ki.neuronsimulator.core.model.Layer;
import pl.edu.agh.ki.neuronsimulator.core.model.NeuralNet;
import pl.edu.agh.ki.neuronsimulator.core.model.Neuron;
import pl.edu.agh.ki.neuronsimulator.core.model.NeuronConnection;
import pl.edu.agh.ki.neuronsimulator.core.model.teaching.backpropagation.BackpropagationConnection;
import pl.edu.agh.ki.neuronsimulator.core.model.teaching.backpropagation.BackpropagationLayer;
import pl.edu.agh.ki.neuronsimulator.core.model.teaching.backpropagation.BackpropagationNet;
import pl.edu.agh.ki.neuronsimulator.core.model.teaching.backpropagation.BackpropagationNeuron;

public class BackpropagationNeuronFactory {

	private Map<Neuron, BackpropagationNeuron> neuronMapping = new HashMap<>();

	public BackpropagationNet build(NeuralNet net) {
		BackpropagationNet backpropagationNet = new BackpropagationNet(net);

		createLayers(backpropagationNet);
		createNeurons(backpropagationNet);
		createConnections(backpropagationNet);

		return backpropagationNet;
	}

	private void createConnections(BackpropagationNet backpropagationNet) {
		for (NeuronConnection conn : getAllConnections(backpropagationNet)) {
			BackpropagationConnection backpropagationConnection = new BackpropagationConnection(conn);

			Neuron beginNeuron = conn.getBeginNeuron();
			addConnectionToNeruon(beginNeuron, backpropagationConnection);
			backpropagationConnection.setBeginNeuron(neuronMapping.get(beginNeuron));

			Neuron endNeuron = conn.getEndNeuron();
			addConnectionToNeruon(endNeuron, backpropagationConnection);
			backpropagationConnection.setEndNeuron(neuronMapping.get(endNeuron));
		}
	}

	private void addConnectionToNeruon(Neuron neuron, BackpropagationConnection connection) {
		neuronMapping.get(neuron).addConnection(connection);
	}

	private Set<NeuronConnection> getAllConnections(BackpropagationNet backpropagationNet) {
		List<NeuronConnection> allNeuronConnections = backpropagationNet.getNeuralNet().getAllNeuronConnections();
		return new HashSet<NeuronConnection>(allNeuronConnections);
	}

	private void createNeurons(BackpropagationNet backpropagationNet) {
		for (BackpropagationLayer layer : backpropagationNet.getBackpropagationLayers()) {
			for (Neuron neuron : layer.getLayer().getNeurons()) {
				BackpropagationNeuron backpropagationNeuron = new BackpropagationNeuron(neuron);
				layer.addNeuron(backpropagationNeuron);
				neuronMapping.put(neuron, backpropagationNeuron);
			}
		}
	}

	private void createLayers(BackpropagationNet backpropagationNet) {
		NeuralNet net = backpropagationNet.getNeuralNet();

		for (Layer layer : net.getLayers()) {
			backpropagationNet.addLayer(new BackpropagationLayer(layer));
		}
	}
}
