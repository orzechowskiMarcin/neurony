package pl.edu.agh.ki.neuronsimulator.core.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;
import java.util.UUID;

public class NeuralNet {

	private final TreeMap<Integer, Layer> layers;

	public NeuralNet(Map<Integer, Layer> layers) {
		this.layers = new TreeMap<Integer, Layer>(layers);
	}

	public Collection<Layer> getLayers() {
		return layers.values();
	}

	public List<Neuron> getAllNeurons() {
		List<Neuron> neurons = new ArrayList<>();
		for (Layer layer : getLayers()) {
			neurons.addAll(layer.getNeurons());
		}
		return neurons;
	}

	public List<NeuronConnection> getAllNeuronConnections() {
		List<NeuronConnection> neuronConnections = new ArrayList<>();
		for (Layer layer : getLayers()) {
			neuronConnections.addAll(layer.getAllNeuronConnections());
		}
		return neuronConnections;
	}

	/**
	 * 
	 * @return amount of layers
	 */
	public int getLayersSize() {
		return layers.size();
	}

	/**
	 * 
	 * @return maximal amount of neurons in layer
	 */
	public int getNuronsSize() {
		int neuronsSize = 0;
		for (Layer layer : getLayers()) {
			if (neuronsSize < layer.getNeuronSize()) {
				neuronsSize = layer.getNeuronSize();
			}
		}
		return neuronsSize;
	}

	public void compute() {
		UUID currentComputationID = UUID.randomUUID();
		lastLayer().compute(currentComputationID);
	}

	public Layer lastLayer() {
		return layers.lastEntry().getValue();
	}

	public Layer firstLayer() {
		return layers.firstEntry().getValue();
	}

	public void randomizeBiasses(double from, double to) {
		Random rand = new Random();

		for (Neuron neuron : getAllNeurons()) {
			neuron.setBiasAsDouble(getRandomDouble(from, to, rand));
		}
	}

	public void randoimzeWeights(double from, double to) {
		Random rand = new Random();

		for (NeuronConnection conn : getAllNeuronConnections()) {
			conn.setWegiht(getRandomDouble(from, to, rand));
		}
	}

	private double getRandomDouble(double from, double to, Random rand) {
		if (from == to) {
			return from;
		}

		double result = rand.nextDouble();
		result *= (to - from);
		result += from;
		return result;
	}

}
