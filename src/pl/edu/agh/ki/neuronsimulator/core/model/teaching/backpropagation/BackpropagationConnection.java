package pl.edu.agh.ki.neuronsimulator.core.model.teaching.backpropagation;

import pl.edu.agh.ki.neuronsimulator.core.model.NeuronConnection;
import pl.edu.agh.ki.neuronsimulator.core.model.teaching.backpropagation.algorithm.Backpropagations;
import pl.edu.agh.ki.neuronsimulator.core.model.teaching.backpropagation.algorithm.BackpropagationsParams;

public class BackpropagationConnection {

	private NeuronConnection neuronConnection;
	private double previousWeightChange = 0.0;
	private BackpropagationNeuron beginNeuron;
	private BackpropagationNeuron endNeuron;

	public BackpropagationConnection(NeuronConnection neuronConnection) {
		this.neuronConnection = neuronConnection;
	}

	public NeuronConnection getNeuronConnection() {
		return neuronConnection;
	}

	public void setBeginNeuron(BackpropagationNeuron beginNeuron) {
		this.beginNeuron = beginNeuron;
	}

	public void setEndNeuron(BackpropagationNeuron endNeuron) {
		this.endNeuron = endNeuron;
	}

	public void teach(BackpropagationsParams params) {
		BackpropagationsParams newParams = params.withPreviousWeightChange(previousWeightChange).withInputSignal(
				neuronConnection.getBeginNeuron().getValueAsDouble());

		double weightDelta = Backpropagations.computeWeightChange(newParams);
		previousWeightChange = weightDelta;
		double newWeight = neuronConnection.getWeightAsDouble() + weightDelta;
		neuronConnection.setWegiht(newWeight);
	}

	public double getFaultMultipledByWeight() {
		return neuronConnection.getWeight() * endNeuron.getNeuronFault();
	}

}
