package pl.edu.agh.ki.neuronsimulator.core.factory;

public class NeuronConnectionDescription {

	private NeuronDescription beginNeuron = new NeuronDescription();
	private NeuronDescription endNeuron = new NeuronDescription();

	public int getBeginNeuronLayer() {
		return beginNeuron.getNeuronLayer();
	}

	public NeuronConnectionDescription setBeginNeuronLayer(int beginNeuronLayer) {
		this.beginNeuron.setNeuronLayer(beginNeuronLayer);
		return this;
	}

	public NeuronConnectionDescription setBeginNeuronLayer(String beginNeuronLayer) {
		return setBeginNeuronLayer(Integer.parseInt(beginNeuronLayer));
	}

	public int getBeginNeuronNumber() {
		return beginNeuron.getNeuronNumber();
	}

	public NeuronConnectionDescription setBeginNeuronNumber(int beginNeuronNumber) {
		this.beginNeuron.setNeuronNumber(beginNeuronNumber);
		return this;
	}

	public NeuronConnectionDescription setBeginNeuronNumber(String beginNeuronNumber) {
		return setBeginNeuronNumber(Integer.parseInt(beginNeuronNumber));
	}

	public int getEndNeuronLayer() {
		return endNeuron.getNeuronLayer();
	}

	public NeuronConnectionDescription setEndNeuronLayer(int endNeuronLayer) {
		this.endNeuron.setNeuronLayer(endNeuronLayer);
		return this;
	}

	public NeuronConnectionDescription setEndNeuronLayer(String endNeuronLayer) {
		return setEndNeuronLayer(Integer.parseInt(endNeuronLayer));
	}

	public int getEndNeuronNumber() {
		return endNeuron.getNeuronNumber();
	}

	public NeuronConnectionDescription setEndNeuronNumber(int endNeuronNumber) {
		this.endNeuron.setNeuronNumber(endNeuronNumber);
		return this;
	}

	public NeuronConnectionDescription setEndNeuronNumber(String endNeuronNumber) {
		return setEndNeuronNumber(Integer.parseInt(endNeuronNumber));
	}

	public NeuronConnectionDescription setBeginNeuron(NeuronDescription beginNeuron) {
		this.beginNeuron = beginNeuron;
		return this;
	}

	public NeuronConnectionDescription setEndNeuron(NeuronDescription endNeuron) {
		this.endNeuron = endNeuron;
		return this;
	}

	public NeuronDescription getBeginNeuron() {
		return beginNeuron;
	}

	public NeuronDescription getEndNeuron() {
		return endNeuron;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof NeuronConnectionDescription)) {
			return false;
		}

		NeuronConnectionDescription other = (NeuronConnectionDescription) obj;
		boolean result = this.beginNeuron.equals(other.beginNeuron);
		result &= this.endNeuron.equals(other.endNeuron);
		return result;
	}

	@Override
	public int hashCode() {
		int hashCode = beginNeuron.hashCode() / 2 + endNeuron.hashCode() / 2;
		return hashCode;
	}
}
