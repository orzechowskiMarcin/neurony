package pl.edu.agh.ki.neuronsimulator.core.model.teaching.backpropagation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.edu.agh.ki.neuronsimulator.core.model.Layer;
import pl.edu.agh.ki.neuronsimulator.core.model.Neuron;
import pl.edu.agh.ki.neuronsimulator.core.model.teaching.backpropagation.algorithm.BackpropagationsParams;

public class BackpropagationLayer {

	private Map<Neuron, BackpropagationNeuron> neuronsMapping = new HashMap<Neuron, BackpropagationNeuron>();
	private Layer layer;

	public BackpropagationLayer(Layer layer) {
		this.layer = layer;
	}

	public void addNeuron(BackpropagationNeuron neuron) {
		neuronsMapping.put(neuron.getNeuron(), neuron);
	}

	public Layer getLayer() {
		return layer;
	}

	public void teach(double[] valusForOutputLayer, BackpropagationsParams parmas) {
		int neuronNumber = 0;
		for (BackpropagationNeuron neuron : getNeuronsInCorrectOrder()) {
			BackpropagationsParams newParams = parmas.withExpectingOutputSignal(valusForOutputLayer[neuronNumber]).withActivationFunction(
					layer.getActivationFunction());
			neuron.teach(newParams);

			neuronNumber++;
		}
	}

	public void teach(BackpropagationsParams parmas) {
		for (BackpropagationNeuron neuron : getNeuronsInCorrectOrder()) {
			BackpropagationsParams newParams = parmas.withActivationFunction(layer.getActivationFunction());
			neuron.teach(newParams);
		}
	}

	private List<BackpropagationNeuron> getNeuronsInCorrectOrder() {
		List<BackpropagationNeuron> neuronsInCorrectOrder = new ArrayList<>();
		for (Neuron neuron : layer.getNeurons()) {
			neuronsInCorrectOrder.add(neuronsMapping.get(neuron));
		}
		return neuronsInCorrectOrder;
	}

}
