package pl.edu.agh.ki.neuronsimulator.core.model.teaching.countepropagation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.edu.agh.ki.neuronsimulator.core.model.Layer;
import pl.edu.agh.ki.neuronsimulator.core.model.Neuron;

public class CounterpropagationLayer {

	private Layer layer;
	private Map<Neuron, CounterpropagationNeuron> neuronsMapping = new HashMap<>();

	public CounterpropagationLayer(Layer layer) {
		this.layer = layer;
	}

	public void addNeuron(CounterpropagationNeuron neuron) {
		neuronsMapping.put(neuron.getNeuron(), neuron);
	}

	public void teach(double[] valusForOutputLayer, double learningSpeed) {
		int valueNumber = 0;
		for (CounterpropagationNeuron neuron : getNeuronsInCorrectOrder()) {
			neuron.teach(valusForOutputLayer[valueNumber], learningSpeed);
			valueNumber++;
		}
	}

	private List<CounterpropagationNeuron> getNeuronsInCorrectOrder() {
		List<CounterpropagationNeuron> neuronsInCorrectOrder = new ArrayList<>();
		for (Neuron neuron : layer.getNeurons()) {
			neuronsInCorrectOrder.add(neuronsMapping.get(neuron));
		}
		return neuronsInCorrectOrder;
	}

}
