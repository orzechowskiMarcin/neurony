package pl.edu.agh.ki.neuronsimulator.gui.handlers;

import pl.edu.agh.ki.neuronsimulator.core.exceptions.NeuralNetSchemaException;
import pl.edu.agh.ki.neuronsimulator.core.factory.CounterpropagationNetFactory;
import pl.edu.agh.ki.neuronsimulator.core.model.NeuralNet;
import pl.edu.agh.ki.neuronsimulator.core.model.teaching.countepropagation.CounterpropagationNet;
import pl.edu.agh.ki.neuronsimulator.gui.Activator;

public class CounterpropagationNetLoadHandler extends AbstractNeuralNetLoadHandler {

	@Override
	protected void onNeuralNetBroadCasted(NeuralNet neuralNet) throws NeuralNetSchemaException {
		CounterpropagationNetFactory factory = new CounterpropagationNetFactory();
		CounterpropagationNet counterpropagationNet = factory.build(neuralNet);
		Activator
				.getDefault()
				.getPreferenceStore()
				.firePropertyChangeEvent(Activator.COUNTERPROPAGATION_TEACHER_LOADED, new Object(), counterpropagationNet);

	}
}
