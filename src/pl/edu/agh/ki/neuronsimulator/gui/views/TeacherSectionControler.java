package pl.edu.agh.ki.neuronsimulator.gui.views;

import java.util.List;

import pl.edu.agh.ki.neuronsimulator.core.exceptions.CannotTeachNetException;
import pl.edu.agh.ki.neuronsimulator.core.model.teaching.LearnigSpeedDTO;
import pl.edu.agh.ki.neuronsimulator.core.model.teaching.Teacher;
import pl.edu.agh.ki.neuronsimulator.core.parser.InputDataFileParser;

public class TeacherSectionControler {

	private String inputDataFileName;
	private Teacher teacher;

	public void setInputDataFileName(String fileName) {
		this.inputDataFileName = fileName;
	}

	public void teachNeuralNet(double learningSpeed, double momentum, int epochAmount) throws CannotTeachNetException {
		if (inputDataFileName == null || inputDataFileName.isEmpty()) {
			throw new CannotTeachNetException("Specify file with input data");
		}

		List<Double[]> parsedInputData = InputDataFileParser.parse(inputDataFileName);
		LearnigSpeedDTO speedsDTO = new LearnigSpeedDTO(learningSpeed, momentum);
		teacher.teach(parsedInputData, speedsDTO, epochAmount);
	}

	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}

}
