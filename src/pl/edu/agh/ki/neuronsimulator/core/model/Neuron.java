package pl.edu.agh.ki.neuronsimulator.core.model;

import java.awt.geom.Point2D;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public class Neuron {

	protected final List<NeuronConnection> inConnections = new ArrayList<>();
	private final List<NeuronConnection> outConnections = new ArrayList<>();
	private BigDecimal value;
	private BigDecimal allInputsSum = BigDecimal.ZERO;
	private double bias;
	private boolean biasDefined;
	private boolean valueSetted = false;
	private final int numberInLayer;
	private UUID lastComputationUUID;
	private Layer layer;

	private double verticalPosition;
	private double horizontalPosition;

	public Neuron(Layer layer, int numberInLayer, double bias) {
		this.layer = layer;
		this.numberInLayer = numberInLayer;
		this.bias = bias;
		this.biasDefined = bias != 0.0 ? true : false;
	}

	public BigDecimal getAllInputsSum() {
		return allInputsSum;
	}

	public void addInConnection(NeuronConnection connection) {
		inConnections.add(connection);
	}

	public void addOutConnection(NeuronConnection connection) {
		outConnections.add(connection);
	}

	public BigDecimal getValue() {
		return value;
	}

	public double getValueAsDouble() {
		return value.doubleValue();
	}

	public void setValue(BigDecimal value) {
		this.value = value;
		valueSetted = true;
	}

	public void setValue(double value) {
		setValue(new BigDecimal(value));
	}

	public boolean isValueSetted() {
		return valueSetted;
	}

	public int getNumberInLayer() {
		return numberInLayer;
	}

	public Set<NeuronConnection> getAllConnections() {
		Set<NeuronConnection> connections = new HashSet<>();
		connections.addAll(inConnections);
		connections.addAll(outConnections);
		return connections;
	}

	public List<NeuronConnection> getInConnections() {
		return inConnections;
	}

	public List<NeuronConnection> getOutConnections() {
		return outConnections;
	}

	public void setVerticalPosition(double verticalPosition) {
		this.verticalPosition = verticalPosition;
	}

	public void setHorizontalPosition(double horizontalPosition) {
		this.horizontalPosition = horizontalPosition;
	}

	public Point2D getLocation() {
		return new Point2D.Double(horizontalPosition, verticalPosition);
	}

	@Override
	public String toString() {
		StringBuilder builder = getNameBuilder();

		if (valueSetted) {
			builder.append(" - ");
			builder.append(value.setScale(4, RoundingMode.HALF_UP).toPlainString());
		}

		return builder.toString();
	}

	protected StringBuilder getNameBuilder() {
		StringBuilder builder = new StringBuilder();
		builder.append("N_");
		builder.append(layer.getLayerNumber());
		builder.append("/");
		builder.append(numberInLayer);
		return builder;
	}

	public String getName() {
		return getNameBuilder().toString();
	}

	public boolean isValueManualyChangeable() {
		return layer.isFirst();
	}

	protected BigDecimal compute(UUID currentComputationUUID) {
		if (inConnections.isEmpty() || currentComputationUUID.equals(lastComputationUUID)) {
			return getValue();
		}
		lastComputationUUID = currentComputationUUID;

		BigDecimal summedValue = BigDecimal.ZERO;

		if (isBiasDefinable()) {
			summedValue = summedValue.add(new BigDecimal(bias));
		}

		for (NeuronConnection conn : inConnections) {
			summedValue = summedValue.add(conn.computeValue(currentComputationUUID));
		}
		allInputsSum = summedValue;

		setComputedValue(summedValue);
		return getValue();
	}

	protected void setComputedValue(BigDecimal computedValue) {
		setValue(layer.getActivationFunction().compute(computedValue));
	}

	public Layer getLayer() {
		return layer;
	}

	public void setBiasAsDouble(double bias) {
		this.bias = bias;
	}

	public boolean isBiasDefinable() {
		return !layer.isFirst();
	}

	public String getBiasAsString() {
		return Double.toString(bias);
	}

	@Override
	public int hashCode() {
		// FIXME zrobione do dupy
		return layer.getLayerNumber() + numberInLayer;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof Neuron)) {
			return false;
		}
		Neuron other = (Neuron) obj;
		return other.getLayer().equals(this.getLayer()) && other.numberInLayer == this.numberInLayer;
	}

	/**
	 * @return the biasDefined
	 */
	public boolean isBiasDefined() {
		return biasDefined;
	}

	/**
	 * @param biasDefined
	 *            the biasDefined to set
	 */
	public void setBiasDefined(boolean biasDefined) {
		this.biasDefined = biasDefined;
	}

	public double getBias() {
		return bias;
	}
}
