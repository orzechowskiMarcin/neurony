package pl.edu.agh.ki.neuronsimulator.core.model.teaching.countepropagation;

import java.util.Arrays;
import java.util.List;

import pl.edu.agh.ki.neuronsimulator.core.exceptions.InvalidInputException;
import pl.edu.agh.ki.neuronsimulator.core.model.NeuralNet;
import pl.edu.agh.ki.neuronsimulator.core.model.teaching.LearnigSpeedDTO;
import pl.edu.agh.ki.neuronsimulator.core.model.teaching.Teacher;
import pl.edu.agh.ki.neuronsimulator.core.model.teaching.kohonen.KohonenNeuralNet;

public class CounterpropagationNet extends Teacher {

	private NeuralNet neuralNet;
	private KohonenNeuralNet kohonenNeuralNet;
	private CounterpropagationLayer counterpropagationLayer;

	public CounterpropagationNet(NeuralNet net) {
		this.neuralNet = net;
	}

	public void setKohonenNeuralNet(KohonenNeuralNet kohonenNeuralNet) {
		this.kohonenNeuralNet = kohonenNeuralNet;
	}

	public void setCounterpropagationLayer(CounterpropagationLayer counterpropagationLayer) {
		this.counterpropagationLayer = counterpropagationLayer;
	}

	public KohonenNeuralNet getKohonenNeuralNet() {
		return kohonenNeuralNet;
	}

	@Override
	protected void teach(double[] inputValuesTab, LearnigSpeedDTO speedsDTO) throws InvalidInputException {
		if (getInputLayerSize() + getOutputLayerSize() != inputValuesTab.length) {
			throw new InvalidInputException("Input size do not match input and output neurons amount.");
		}

		double[] valusForInputLayer = Arrays.copyOfRange(inputValuesTab, 0, getInputLayerSize());
		double[] valusForOutputLayer = Arrays.copyOfRange(inputValuesTab, getInputLayerSize(), inputValuesTab.length);

		kohonenNeuralNet.teach(valusForInputLayer, speedsDTO);
		neuralNet.compute();
		// TODO
		counterpropagationLayer.teach(valusForOutputLayer, speedsDTO.getMomentum());
	}

	private int getOutputLayerSize() {
		return neuralNet.lastLayer().getNeuronSize();
	}

	private int getInputLayerSize() {
		return neuralNet.firstLayer().getNeuronSize();
	}

	@Override
	protected boolean isInputDataSizeCorrect(List<Double[]> parsedInputData) {
		return parsedInputData.iterator().next().length == getInputLayerSize() + getOutputLayerSize();
	}

}
