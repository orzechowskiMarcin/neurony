package pl.edu.agh.ki.neuronsimulator.core.model.activation;

import java.math.BigDecimal;

public abstract class ActivationFunction {
	private double factor;

	public ActivationFunction(double factor) {
		this.factor = factor;
	}

	public abstract BigDecimal compute(BigDecimal argument);

	public abstract double compute(double argument);

	public abstract double computeDerivative(double argument);

	public abstract String getName();

	public double getFactor() {
		return factor;
	}

	public String getFactorAsString() {
		return Double.toString(factor);
	}

	public void setFactor(double factor) {
		this.factor = factor;
	}

	public void setFactor(String factor) {
		setFactor(Double.parseDouble(factor));
	}

	public static abstract class Creator {

		public abstract String getName();

		public abstract ActivationFunction newInstance(double factor);
	}

}
