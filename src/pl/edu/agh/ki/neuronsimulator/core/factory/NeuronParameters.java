package pl.edu.agh.ki.neuronsimulator.core.factory;

public class NeuronParameters {

	private double bias;

	public double getBias() {
		return bias;
	}

	public NeuronParameters setBias(double bias) {
		this.bias = bias;
		return this;
	}

	public NeuronParameters setBias(String bias) {
		return setBias(Double.parseDouble(bias));
	}
}
