package pl.edu.agh.ki.neuronsimulator.core.exceptions;

public class InvalidFormatException extends Exception {

	private static final long serialVersionUID = 1L;

	public InvalidFormatException(String msg) {
		super(msg);
	}
	
	public InvalidFormatException(String msg, Throwable t) {
		super(msg, t);
	}
}
