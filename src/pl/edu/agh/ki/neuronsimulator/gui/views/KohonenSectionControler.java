package pl.edu.agh.ki.neuronsimulator.gui.views;

import java.util.List;

import pl.edu.agh.ki.neuronsimulator.core.exceptions.CannotTeachNetException;
import pl.edu.agh.ki.neuronsimulator.core.model.teaching.LearnigSpeedDTO;
import pl.edu.agh.ki.neuronsimulator.core.model.teaching.kohonen.KohonenNeuralNet;
import pl.edu.agh.ki.neuronsimulator.core.model.teaching.kohonen.neighborhood.OneDimensionNeighborhood;
import pl.edu.agh.ki.neuronsimulator.core.model.teaching.kohonen.neighborhood.TwoDimensionNeighborhood;
import pl.edu.agh.ki.neuronsimulator.core.model.teaching.kohonen.neighborhood.function.FunctionWithDisableCompetitionAndNeigborhood;
import pl.edu.agh.ki.neuronsimulator.core.model.teaching.kohonen.neighborhood.function.FunctionWithDisabledNeigborhood;
import pl.edu.agh.ki.neuronsimulator.core.model.teaching.kohonen.neighborhood.function.NormalNeigborhoodFnction;
import pl.edu.agh.ki.neuronsimulator.core.parser.InputDataFileParser;

public class KohonenSectionControler {
	private KohonenNeuralNet neuralNet;
	private CompetitionState currentCompetitionStates;
	private NeigborhoodType currentNeigborhoodType;
	private String inputDataFileName = "";

	public KohonenSectionControler(KohonenNeuralNet neuralNet) {
		this.neuralNet = neuralNet;

		currentCompetitionStates = CompetitionState.ENABLED;
		currentNeigborhoodType = NeigborhoodType.ONE_DIMENSION;

		this.neuralNet.changeNeighborhood(new OneDimensionNeighborhood());
	}

	public static enum CompetitionState {
		ENABLED("Enabled"), DISABLED("Disabled");

		private String name;

		private CompetitionState(String name) {
			this.name = name;
		}

		@Override
		public String toString() {
			return name;
		}
	}

	public static String[] getCompetitionsStatesAsString() {
		CompetitionState[] states = CompetitionState.values();
		String[] statesAsString = new String[states.length];

		for (int i = 0; i < statesAsString.length; i++) {
			statesAsString[i] = states[i].toString();
		}

		return statesAsString;
	}

	public static enum NeigborhoodType {
		NONE("None"), ONE_DIMENSION("1D"), TWO_DIMENSION("2D");

		private String name;

		private NeigborhoodType(String name) {
			this.name = name;
		}

		@Override
		public String toString() {
			return name;
		}
	}

	public static String[] getNeighborhoodsAsString() {
		NeigborhoodType[] types = NeigborhoodType.values();
		String typesAsString[] = new String[types.length];

		for (int i = 0; i < typesAsString.length; i++) {
			typesAsString[i] = types[i].toString();
		}

		return typesAsString;
	}

	public void neighborhoodTypeChanged(int selectionIndex) {
		if (currentCompetitionStates == CompetitionState.DISABLED) {
			throw new IllegalStateException("Neigborhood cannot be changed when competition is diabled.");
		}

		currentNeigborhoodType = NeigborhoodType.values()[selectionIndex];

		switch (currentNeigborhoodType) {
		case ONE_DIMENSION:
			neuralNet.changeNeighborhood(new OneDimensionNeighborhood());
			neuralNet.changeNeighborhoodFunction(new NormalNeigborhoodFnction());
			break;

		case TWO_DIMENSION:
			neuralNet.changeNeighborhood(new TwoDimensionNeighborhood());
			neuralNet.changeNeighborhoodFunction(new NormalNeigborhoodFnction());
			break;

		// NONE
		default:
			neuralNet.changeNeighborhoodFunction(new FunctionWithDisabledNeigborhood());
			break;
		}
	}

	public void competitionChanged(int selectionIndex) {
		currentCompetitionStates = CompetitionState.values()[selectionIndex];

		if (currentCompetitionStates == CompetitionState.ENABLED && currentNeigborhoodType != NeigborhoodType.NONE) {
			neuralNet.changeNeighborhoodFunction(new NormalNeigborhoodFnction());
		} else if (currentCompetitionStates == CompetitionState.ENABLED && currentNeigborhoodType == NeigborhoodType.NONE) {
			neuralNet.changeNeighborhoodFunction(new FunctionWithDisabledNeigborhood());
		} else if (currentCompetitionStates == CompetitionState.DISABLED) {
			currentNeigborhoodType = NeigborhoodType.NONE;
			neuralNet.changeNeighborhoodFunction(new FunctionWithDisableCompetitionAndNeigborhood());
		}
	}

	public CompetitionState getCurrentCompetitionState() {
		return currentCompetitionStates;
	}

	public NeigborhoodType getCurrentNeigborhoodType() {
		return currentNeigborhoodType;
	}

	public boolean isNeighborhoodTypeEnabled() {
		return currentCompetitionStates != CompetitionState.DISABLED;
	}

	public String getInputDataFileName() {
		return inputDataFileName;
	}

	public void setInputDataFileName(String inputDataFileName) {
		this.inputDataFileName = inputDataFileName;
	}

	public void teachNeuralNet(double learningSpeedValue, int epochAmountValue) throws CannotTeachNetException {
		if (inputDataFileName == null || inputDataFileName.isEmpty()) {
			throw new CannotTeachNetException("Specify file with input data");
		}

		List<Double[]> parsedInputData = InputDataFileParser.parse(inputDataFileName);
		neuralNet.teach(parsedInputData, new LearnigSpeedDTO(learningSpeedValue, 0), epochAmountValue);
	}
}
