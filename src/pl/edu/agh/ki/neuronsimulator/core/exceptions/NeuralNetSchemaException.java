package pl.edu.agh.ki.neuronsimulator.core.exceptions;

public class NeuralNetSchemaException extends Exception {

	private static final long serialVersionUID = 1L;

	public NeuralNetSchemaException(String msg) {
		super(msg);
	}
	
	public NeuralNetSchemaException(String msg, Throwable t){
		super(msg, t);
	}
}
