package pl.edu.agh.ki.neuronsimulator.core.model.teaching.kohonen.neighborhood;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import pl.edu.agh.ki.neuronsimulator.core.exceptions.InvalidNeigborhoodException;
import pl.edu.agh.ki.neuronsimulator.core.model.teaching.kohonen.KohonenNeuron;

public class OneDimensionNeighborhood extends Neighborhood {
	private Map<KohonenNeuron, Integer> neuronToIndexMap;

	@Override
	public void buildNeighborhood(Collection<KohonenNeuron> neurons) {
		neuronToIndexMap = new HashMap<KohonenNeuron, Integer>(neurons.size());

		int i = 0;
		for (KohonenNeuron neuron : neurons) {
			neuronToIndexMap.put(neuron, i);
			i++;
		}
	}

	@Override
	public int getRadius(KohonenNeuron fromNeuron, KohonenNeuron toNeuron) {
		if (fromNeuron.equals(toNeuron)) {
			return 0;
		}

		Integer fromIndex = neuronToIndexMap.get(fromNeuron);
		Integer toIndex = neuronToIndexMap.get(toNeuron);

		if (fromIndex == null || toIndex == null) {
			throw new InvalidNeigborhoodException("Neurons " + fromNeuron.toString() + " and " + toNeuron.toString()
					+ " are not neigborhoods.");
		}

		return Math.abs(fromIndex - toIndex) + 1;
	}

}
