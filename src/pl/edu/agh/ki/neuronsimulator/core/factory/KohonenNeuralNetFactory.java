package pl.edu.agh.ki.neuronsimulator.core.factory;

import pl.edu.agh.ki.neuronsimulator.core.exceptions.NeuralNetSchemaException;
import pl.edu.agh.ki.neuronsimulator.core.model.Layer;
import pl.edu.agh.ki.neuronsimulator.core.model.NeuralNet;
import pl.edu.agh.ki.neuronsimulator.core.model.Neuron;
import pl.edu.agh.ki.neuronsimulator.core.model.activation.implementation.EmptyFunction;
import pl.edu.agh.ki.neuronsimulator.core.model.teaching.kohonen.KohonenLayer;
import pl.edu.agh.ki.neuronsimulator.core.model.teaching.kohonen.KohonenNeuralNet;
import pl.edu.agh.ki.neuronsimulator.core.model.teaching.kohonen.neighborhood.OneDimensionNeighborhood;

public class KohonenNeuralNetFactory {

	public KohonenNeuralNet build(NeuralNet net) throws NeuralNetSchemaException {
		if (net.getLayersSize() != 2) {
			throw new NeuralNetSchemaException("Kohonen Neural Net cannot has " + net.getLayersSize() + " layers.");
		}

		fixNet(net);

		KohonenNeuralNet kohonenNeuralNet = new KohonenNeuralNet();
		kohonenNeuralNet.setInputLayer(net.firstLayer());

		KohonenLayer kohonenLayer = new KohonenLayer(net.lastLayer());
		kohonenNeuralNet.setKohonenLayer(kohonenLayer);
		kohonenLayer.changeNeighborhood(new OneDimensionNeighborhood());

		return kohonenNeuralNet;
	}

	private void fixNet(NeuralNet net) {
		for (Neuron neuron : net.getAllNeurons()) {
			neuron.setBiasDefined(false);
		}
		for (Layer layer : net.getLayers()) {
			layer.changeActivationFunction(new EmptyFunction(0));
		}
	}

}
