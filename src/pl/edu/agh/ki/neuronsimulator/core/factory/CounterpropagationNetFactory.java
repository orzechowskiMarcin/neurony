package pl.edu.agh.ki.neuronsimulator.core.factory;

import java.util.ArrayList;
import java.util.List;

import pl.edu.agh.ki.neuronsimulator.core.exceptions.NeuralNetSchemaException;
import pl.edu.agh.ki.neuronsimulator.core.model.Layer;
import pl.edu.agh.ki.neuronsimulator.core.model.NeuralNet;
import pl.edu.agh.ki.neuronsimulator.core.model.Neuron;
import pl.edu.agh.ki.neuronsimulator.core.model.activation.implementation.EmptyFunction;
import pl.edu.agh.ki.neuronsimulator.core.model.teaching.countepropagation.CounterpropagationLayer;
import pl.edu.agh.ki.neuronsimulator.core.model.teaching.countepropagation.CounterpropagationNet;
import pl.edu.agh.ki.neuronsimulator.core.model.teaching.countepropagation.CounterpropagationNeuron;
import pl.edu.agh.ki.neuronsimulator.core.model.teaching.kohonen.KohonenLayer;
import pl.edu.agh.ki.neuronsimulator.core.model.teaching.kohonen.KohonenNeuralNet;
import pl.edu.agh.ki.neuronsimulator.core.model.teaching.kohonen.neighborhood.OneDimensionNeighborhood;
import pl.edu.agh.ki.neuronsimulator.core.model.teaching.kohonen.neighborhood.function.FunctionWithDisabledNeigborhood;

public class CounterpropagationNetFactory {

	public CounterpropagationNet build(NeuralNet net) throws NeuralNetSchemaException {
		if (net.getLayersSize() != 3) {
			throw new NeuralNetSchemaException("Counterpropagation Neural Net cannot has " + net.getLayersSize() + " layers.");
		}

		fixNet(net);

		CounterpropagationNet counterpropagationNet = new CounterpropagationNet(net);

		KohonenNeuralNet kohonenNeuralNet = createKohonenNeuralNet(net);
		fixKohonenNet(kohonenNeuralNet);
		counterpropagationNet.setKohonenNeuralNet(kohonenNeuralNet);

		CounterpropagationLayer counterpropagationLayer = createCounterpropagationLayer(net.lastLayer());
		counterpropagationNet.setCounterpropagationLayer(counterpropagationLayer);

		return counterpropagationNet;
	}

	private CounterpropagationLayer createCounterpropagationLayer(Layer layer) {
		CounterpropagationLayer counterpropagationLayer = new CounterpropagationLayer(layer);

		for (Neuron neuron : layer.getNeurons()) {
			counterpropagationLayer.addNeuron(new CounterpropagationNeuron(neuron));
		}

		return counterpropagationLayer;
	}

	private KohonenNeuralNet createKohonenNeuralNet(NeuralNet net) {
		KohonenNeuralNet kohonenNeuralNet = new KohonenNeuralNet();
		kohonenNeuralNet.setInputLayer(net.firstLayer());

		KohonenLayer kohonenLayer = new KohonenLayer(getMiddleLayer(net));
		kohonenNeuralNet.setKohonenLayer(kohonenLayer);
		kohonenLayer.changeNeighborhood(new OneDimensionNeighborhood());

		return kohonenNeuralNet;
	}

	private Layer getMiddleLayer(NeuralNet net) {
		List<Layer> layer = new ArrayList<>(net.getLayers());
		return layer.get(1);
	}

	private void fixKohonenNet(KohonenNeuralNet kohonenNeuralNet) {
		kohonenNeuralNet.changeNeighborhoodFunction(new FunctionWithDisabledNeigborhood());
		kohonenNeuralNet.changeNeighborhood(new OneDimensionNeighborhood());
	}

	private void fixNet(NeuralNet net) {
		for (Neuron neuron : net.getAllNeurons()) {
			neuron.setBiasDefined(false);
		}
		for (Layer layer : net.getLayers()) {
			layer.changeActivationFunction(new EmptyFunction(0));
		}
	}

}
