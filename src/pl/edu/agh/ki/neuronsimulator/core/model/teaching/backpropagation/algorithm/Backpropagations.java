package pl.edu.agh.ki.neuronsimulator.core.model.teaching.backpropagation.algorithm;

public class Backpropagations {

	public static double computeWeightChange(BackpropagationsParams params) {
		return params.getLearningSpeedFactor() * computeFault(params) * computeDerivative(params) * params.getInputSignal()
				+ computeMomentum(params);
	}

	private static double computeDerivative(BackpropagationsParams params) {
		return params.getActivationFunction().computeDerivative(params.getInputSum());
	}

	public static double computeFault(BackpropagationsParams params) {
		if (params.isHiden()) {
			return computeHidenLayerFault(params);
		}
		return computeOutputLayerFault(params);
	}

	private static double computeMomentum(BackpropagationsParams params) {
		return params.getMomentum() * params.getPreviousWeightChange();
	}

	private static double computeOutputLayerFault(BackpropagationsParams params) {
		return params.getExpectingOutputSignal() - params.getActivationFunction().compute(params.getInputSum());
	}

	private static double computeHidenLayerFault(BackpropagationsParams params) {
		return params.getOutputWeights();
	}
}
