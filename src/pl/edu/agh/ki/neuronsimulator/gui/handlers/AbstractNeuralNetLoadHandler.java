package pl.edu.agh.ki.neuronsimulator.gui.handlers;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.handlers.HandlerUtil;

import pl.edu.agh.ki.neuronsimulator.core.exceptions.NeuralNetSchemaException;
import pl.edu.agh.ki.neuronsimulator.core.factory.FastForwardNeuralNetFactory;
import pl.edu.agh.ki.neuronsimulator.core.model.NeuralNet;
import pl.edu.agh.ki.neuronsimulator.core.parser.NeuronFileParser;
import pl.edu.agh.ki.neuronsimulator.gui.Activator;

public abstract class AbstractNeuralNetLoadHandler extends AbstractHandler {
	private static final String[] SUPPORTED_EXTENSSIONS = new String[] { "*" };

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		Shell shell = HandlerUtil.getActiveWorkbenchWindow(event).getShell();
		showOpenFileDialog(shell);
		return null;
	}

	private void showOpenFileDialog(Shell shell) {
		FileDialog dialog = new FileDialog(shell, SWT.OPEN);
		dialog.setText("Import Neural Net");
		dialog.setFilterExtensions(SUPPORTED_EXTENSSIONS);
		String fileName = dialog.open();
		if (fileName != null) {
			openFile(fileName, shell);
		}
	}

	private void openFile(final String fileName, final Shell shell) {
		ProgressMonitorDialog dialog = new ProgressMonitorDialog(shell);
		try {
			dialog.run(true, true, new IRunnableWithProgress() {

				@Override
				public void run(IProgressMonitor monitor) throws InvocationTargetException, InterruptedException {
					try {
						monitor.beginTask("Importing neural net definition.", 1);
						monitor.subTask("Parsing neural net file.");

						final NeuralNet net = parseFile(fileName);

						monitor.worked(1);
						monitor.done();
						Display.getDefault().asyncExec(new Runnable() {
							@Override
							public void run() {
								Activator
										.getDefault()
										.getPreferenceStore()
										.firePropertyChangeEvent(Activator.NEURAL_NET_LOADED, new Object(), net);

								try {
									onNeuralNetBroadCasted(net);
								} catch (NeuralNetSchemaException e) {
									e.printStackTrace();
								}
							}
						});

					} catch (IOException | NeuralNetSchemaException e) {
						showErrorBox(e.getMessage(), shell);
						e.printStackTrace();
					}
				}

			});
		} catch (InvocationTargetException | InterruptedException e) {
			showErrorBox(e.getMessage(), shell);
			e.printStackTrace();
		}
	}

	private void showErrorBox(final String msg, final Shell shell) {
		Display.getDefault().asyncExec(new Runnable() {

			@Override
			public void run() {
				MessageBox box = new MessageBox(shell, SWT.ICON_ERROR | SWT.OK);
				box.setText(msg);
				box.open();
			}
		});
	}

	protected NeuralNet parseFile(String fileName) throws IOException, NeuralNetSchemaException {
		return NeuronFileParser.parse(fileName, new FastForwardNeuralNetFactory());
	}

	protected void onNeuralNetBroadCasted(NeuralNet neuralNet) throws NeuralNetSchemaException {
		// override if needed
	}
}
