package pl.edu.agh.ki.neuronsimulator.core.model.teaching.backpropagation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import pl.edu.agh.ki.neuronsimulator.core.exceptions.InvalidInputException;
import pl.edu.agh.ki.neuronsimulator.core.model.Layer;
import pl.edu.agh.ki.neuronsimulator.core.model.NeuralNet;
import pl.edu.agh.ki.neuronsimulator.core.model.teaching.LearnigSpeedDTO;
import pl.edu.agh.ki.neuronsimulator.core.model.teaching.Teacher;
import pl.edu.agh.ki.neuronsimulator.core.model.teaching.backpropagation.algorithm.BackpropagationsParams;

public class BackpropagationNet extends Teacher {

	private Map<Layer, BackpropagationLayer> layersMapping = new HashMap<>();
	private NeuralNet neuralNet;

	public BackpropagationNet(NeuralNet net) {
		this.neuralNet = net;
	}

	public void addLayer(BackpropagationLayer layer) {
		layersMapping.put(layer.getLayer(), layer);
	}

	public NeuralNet getNeuralNet() {
		return neuralNet;
	}

	public Collection<BackpropagationLayer> getBackpropagationLayers() {
		return layersMapping.values();
	}

	@Override
	protected boolean isInputDataSizeCorrect(List<Double[]> parsedInputData) {
		return parsedInputData.iterator().next().length == getInputLayerSize() + getOutputLayerSize();
	}

	@Override
	protected void teach(double[] inputValuesTab, LearnigSpeedDTO speedsDTO) throws InvalidInputException {
		if (getInputLayerSize() + getOutputLayerSize() != inputValuesTab.length) {
			throw new InvalidInputException("Input size do not match input and output neurons amount.");
		}

		double[] valusForInputLayer = Arrays.copyOfRange(inputValuesTab, 0, getInputLayerSize());
		double[] valusForOutputLayer = Arrays.copyOfRange(inputValuesTab, getInputLayerSize(), inputValuesTab.length);

		setInputValues(valusForInputLayer);

		neuralNet.compute();
		teachOutputLayer(valusForOutputLayer, speedsDTO);
		teachHidenLayers(speedsDTO);
	}

	private void teachOutputLayer(double[] valusForOutputLayer, LearnigSpeedDTO speedsDTO) {
		BackpropagationsParams parmas = BackpropagationsParams
				.build(false)
				.withLearningSpeedFactor(speedsDTO.getLearningSpeed())
				.withMomentum(speedsDTO.getMomentum());

		getOutputLayer().teach(valusForOutputLayer, parmas);

	}

	private void teachHidenLayers(LearnigSpeedDTO speedsDTO) {
		BackpropagationsParams parmas = BackpropagationsParams
				.build(true)
				.withLearningSpeedFactor(speedsDTO.getLearningSpeed())
				.withMomentum(speedsDTO.getMomentum());

		for (BackpropagationLayer layer : getLayersWithoutOutputLayerInReversOrder()) {
			layer.teach(parmas);
		}
	}

	private List<BackpropagationLayer> getLayersWithoutOutputLayerInReversOrder() {
		Collection<Layer> allLayers = new ArrayList<Layer>(neuralNet.getLayers());
		allLayers.remove(neuralNet.lastLayer());
		allLayers.remove(neuralNet.firstLayer());

		Iterator<Layer> layersReverseIterator = new LinkedList<>(allLayers).descendingIterator();
		List<BackpropagationLayer> backpropagationLayers = new ArrayList<>();
		while (layersReverseIterator.hasNext()) {
			backpropagationLayers.add(layersMapping.get(layersReverseIterator.next()));
		}

		return backpropagationLayers;
	}

	private void setInputValues(double[] inputValues) throws InvalidInputException {
		neuralNet.firstLayer().setValues(inputValues);
	}

	private int getInputLayerSize() {
		return neuralNet.firstLayer().getNeuronSize();
	}

	private int getOutputLayerSize() {
		return neuralNet.lastLayer().getNeuronSize();
	}

	private BackpropagationLayer getOutputLayer() {
		return layersMapping.get(neuralNet.lastLayer());
	}
}
