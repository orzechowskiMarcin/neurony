package pl.edu.agh.ki.neuronsimulator.core.model.activation;

import pl.edu.agh.ki.neuronsimulator.core.model.activation.implementation.EmptyFunction;
import pl.edu.agh.ki.neuronsimulator.core.model.activation.implementation.LinearFunction;
import pl.edu.agh.ki.neuronsimulator.core.model.activation.implementation.SigmoidalFuntion;
import pl.edu.agh.ki.neuronsimulator.core.model.activation.implementation.StepFunction;
import pl.edu.agh.ki.neuronsimulator.gui.exceptions.InputNumberFormatException;

public class ActivationFunctionFactory {
	//@formatter:off
	private static final ActivationFunction.Creator[] availableFunctions = new ActivationFunction.Creator[] { 
		new LinearFunction.Creator(), 
		new SigmoidalFuntion.Creator(),
		new StepFunction.Creator(),
		new EmptyFunction.Creator()
	};
	//@formatter:on

	private double factor;
	private int selctedFunctionIndex = 0;

	public String[] getAvailableFunctionsAsString() {
		String[] functions = new String[availableFunctions.length];
		for (int funcIndex = 0; funcIndex < availableFunctions.length; funcIndex++) {
			functions[funcIndex] = availableFunctions[funcIndex].getName();
		}
		return functions;
	}

	public ActivationFunction getFunctionForSelectedIndex(int index) {
		return availableFunctions[index].newInstance(factor);
	}

	public double getFactor() {
		return factor;
	}

	public void setFactor(double factor) {
		this.factor = factor;
	}

	public void setSelectedFunction(ActivationFunction activationFunction) {
		for (int funcIndex = 0; funcIndex < availableFunctions.length; funcIndex++) {
			if (availableFunctions[funcIndex].getName().equals(activationFunction.getName())) {
				selctedFunctionIndex = funcIndex;
				break;
			}
		}

		factor = activationFunction.getFactor();
	}

	public int getSelctedFunctionIndex() {
		return selctedFunctionIndex;
	}

	public void setSelctedFunctionIndex(int selctedFunctionIndex) {
		this.selctedFunctionIndex = selctedFunctionIndex;
	}

	public void setFactor(String factor) throws InputNumberFormatException {
		try {
			setFactor(Double.parseDouble(factor));
		} catch (NumberFormatException e) {
			throw new InputNumberFormatException("Activation Funtion Factor", e);
		}
	}
}
