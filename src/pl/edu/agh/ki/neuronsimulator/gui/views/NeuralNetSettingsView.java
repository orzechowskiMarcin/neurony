package pl.edu.agh.ki.neuronsimulator.gui.views;

import java.math.BigDecimal;

import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import pl.edu.agh.ki.neuronsimulator.core.exceptions.CannotTeachNetException;
import pl.edu.agh.ki.neuronsimulator.core.model.Layer;
import pl.edu.agh.ki.neuronsimulator.core.model.NeuralNet;
import pl.edu.agh.ki.neuronsimulator.core.model.Neuron;
import pl.edu.agh.ki.neuronsimulator.core.model.NeuronConnection;
import pl.edu.agh.ki.neuronsimulator.core.model.activation.ActivationFunctionFactory;
import pl.edu.agh.ki.neuronsimulator.core.model.teaching.Teacher;
import pl.edu.agh.ki.neuronsimulator.core.model.teaching.countepropagation.CounterpropagationNet;
import pl.edu.agh.ki.neuronsimulator.core.model.teaching.kohonen.KohonenNeuralNet;
import pl.edu.agh.ki.neuronsimulator.gui.Activator;
import pl.edu.agh.ki.neuronsimulator.gui.exceptions.InputNumberFormatException;
import pl.edu.agh.ki.neuronsimulator.gui.layouts.ColumnLayout;

public class NeuralNetSettingsView extends NeuronViewPart {
	public static final String ID = "pl.edu.agh.ki.neuronsimulator.gui.views.NeuralNetSettingsView";

	private Composite composite;
	private IPropertyChangeListener eventListenr;
	private Neuron selectedNeuron;
	private NeuronConnection selectedNeuronConnection;
	private Layer selectedLayer;
	private NeuralNet neuralNet;
	private ActivationFunctionFactory activationFunctionFactory = new ActivationFunctionFactory();

	private Text vertexName;
	private Text vertexValue;
	private Text vertexBias;
	private Button biasActive;

	private Text edgeName;
	private Text edgeWeight;

	private Text layerActivationFunctionFactor;
	private Combo layerActivationFunction;
	private Text layerName;
	private Button saveDataToLayerButton;

	private Text fromRange;
	private Text toRange;
	private Button randomizeBiases;

	private KohonenSectionControler kohonenSectionControler;
	private Combo neigborhoodType;
	private Combo competition;

	private Text learningSpeed;
	private Text momentum;
	private Text inputDataFile;
	private Button browseFilesButton;
	private Button teachButton;
	private Text epochAmount;

	private TeacherSectionControler teacherSectionControler = new TeacherSectionControler();

	public NeuralNetSettingsView() {
		super();

		eventListenr = new IPropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent event) {
				Object targetObject = event.getNewValue();
				try {
					if (event.getProperty().equals(Activator.EDGE_PICKED)) {
						if (targetObject instanceof NeuronConnection) {
							saveDataToNeronConnection();
							selectedNeuronConnection = (NeuronConnection) targetObject;
							fillEdgeData();
						}
					} else if (event.getProperty().equals(Activator.VERTEX_PICKED)) {
						saveDataToNeuron();
						saveDataToLayer();

						selectedNeuron = (Neuron) targetObject;
						selectedLayer = selectedNeuron.getLayer();

						fillNeuronData();
						fillLayerData();
					} else if (event.getProperty().equals(Activator.NEURAL_NET_LOADED)) {
						neuralNet = (NeuralNet) targetObject;
						enableKohonenSection(false);
						enableLayerSection(true);
						enableBiasOptions(true);
						enableTeacherSection(false);
					} else if (event.getProperty().equals(Activator.BACKPROPAGATION_TEACHER_LOADED)) {
						teacherSectionControler.setTeacher((Teacher) targetObject);
						enableTeacherSection(true);
					} else if (event.getProperty().equals(Activator.KOHONENE_TEACHER_LOADED)) {
						teacherSectionControler.setTeacher((Teacher) targetObject);
						kohonenSectionControler = new KohonenSectionControler((KohonenNeuralNet) targetObject);
						enableKohonenSection(true);
						enableBiasOptions(false);
						enableLayerSection(false);
						enableTeacherSection(true);
						repaintKohonenSection();
					} else if (event.getProperty().equals(Activator.COUNTERPROPAGATION_TEACHER_LOADED)) {
						teacherSectionControler.setTeacher((Teacher) targetObject);
						kohonenSectionControler = new KohonenSectionControler(((CounterpropagationNet) targetObject).getKohonenNeuralNet());
						enableTeacherSection(true);
						enableBiasOptions(false);
						enableKohonenSection(true);
					}
				} catch (InputNumberFormatException e) {
					reportError("Saving Error", e.getMessage(), e);
				}
			}

		};
	}

	private void enableTeacherSection(boolean enabled) {
		learningSpeed.setEnabled(enabled);
		momentum.setEnabled(enabled);
		inputDataFile.setEnabled(enabled);
		browseFilesButton.setEnabled(enabled);
		teachButton.setEnabled(enabled);
		epochAmount.setEnabled(enabled);
	}

	@Override
	public void createPartControl(Composite parent) {
		Activator.getDefault().getPreferenceStore().addPropertyChangeListener(eventListenr);

		composite = new Composite(parent, SWT.BORDER);
		composite.setLayout(new ColumnLayout());

		createNeuronNetToolsSection();
		createNeuronSection();
		createLayerSection();
		createNeuronConnectionSection();
		createKohonenSection();
		createTeachSection();
	}

	private void createKohonenSection() {
		final Composite kohonenComposite = new Composite(composite, SWT.BORDER);

		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		kohonenComposite.setLayout(layout);

		Label title = new Label(kohonenComposite, SWT.BOLD | SWT.CENTER);
		title.setText("Kohonen Neural Net Tools");
		title.setLayoutData(newFillGridData(3));

		neigborhoodType = createComboProperty(kohonenComposite, newFillGridData(2), "Neigborhood:",
				KohonenSectionControler.getNeighborhoodsAsString());
		neigborhoodType.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				kohonenSectionControler.neighborhoodTypeChanged(neigborhoodType.getSelectionIndex());
				repaintKohonenSection();
			}
		});

		competition = createComboProperty(kohonenComposite, newFillGridData(2), "Competition:",
				KohonenSectionControler.getCompetitionsStatesAsString());
		competition.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				kohonenSectionControler.competitionChanged(competition.getSelectionIndex());
				repaintKohonenSection();
			}
		});

		enableKohonenSection(false);
	}

	private void repaintKohonenSection() {
		neigborhoodType.setText(kohonenSectionControler.getCurrentNeigborhoodType().toString());
		neigborhoodType.setEnabled(kohonenSectionControler.isNeighborhoodTypeEnabled());
		competition.setText(kohonenSectionControler.getCurrentCompetitionState().toString());
		inputDataFile.setText(kohonenSectionControler.getInputDataFileName());
	}

	private void enableKohonenSection(boolean enabled) {
		neigborhoodType.setEnabled(enabled);
		competition.setEnabled(enabled);
	}

	private void createTeachSection() {
		final Composite teachingSectionComposite = new Composite(composite, SWT.BORDER);

		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		teachingSectionComposite.setLayout(layout);

		Label title = new Label(teachingSectionComposite, SWT.BOLD | SWT.CENTER);
		title.setText("Teach Section");
		title.setLayoutData(newFillGridData(3));

		learningSpeed = createTextProperty(teachingSectionComposite, newFillGridData(2), "Learning Speed:");

		momentum = createTextProperty(teachingSectionComposite, newFillGridData(2), "Momentum/Learning Speed 2:");

		epochAmount = createTextProperty(teachingSectionComposite, newFillGridData(2), "Epoch Amount");

		inputDataFile = createTextProperty(teachingSectionComposite, newFillGridData(), "Input file:");
		inputDataFile.setEditable(false);

		browseFilesButton = createPushButton(teachingSectionComposite, newFillGridData(), "Browse");
		browseFilesButton.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				FileDialog dialog = new FileDialog(teachingSectionComposite.getShell());
				dialog.setText("Open file with input data");
				dialog.setFilterExtensions(new String[] { "*" });
				String fileName = dialog.open();
				if (fileName != null) {
					teacherSectionControler.setInputDataFileName(fileName);
					inputDataFile.setText(fileName);
				}
			}
		});

		teachButton = createPushButton(teachingSectionComposite, newFillGridData(3), "Teach");
		teachButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try {
					double learningSpeedValue = parseNumber(learningSpeed, "Learning Speed");
					double momentumValue = parseNumber(momentum, "Momentum");
					int epochAmountValue = parseIntNumber(epochAmount, "Epoch Amount");

					teacherSectionControler.teachNeuralNet(learningSpeedValue, momentumValue, epochAmountValue);
					repaintGraph();
				} catch (CannotTeachNetException | InputNumberFormatException e1) {
					reportError("Teaching error.", e1.getMessage(), e1);
				}
			}
		});
	}

	private void enableBiasOptions(boolean enabled) {
		vertexBias.setEnabled(enabled);
		randomizeBiases.setEnabled(enabled);
		biasActive.setEnabled(enabled);
	}

	private void enableLayerSection(boolean enabled) {
		layerActivationFunction.setEnabled(enabled);
		layerActivationFunctionFactor.setEnabled(enabled);
		saveDataToLayerButton.setEnabled(enabled);
	}

	private void createNeuronNetToolsSection() {
		Composite toolsComposite = new Composite(composite, SWT.BORDER);

		GridLayout layout = new GridLayout();
		layout.numColumns = 4;
		toolsComposite.setLayout(layout);

		Label title = new Label(toolsComposite, SWT.BOLD | SWT.CENTER);
		title.setText("Neural Net Tools");
		title.setLayoutData(newFillGridData(4));

		new Label(toolsComposite, SWT.LEFT).setText("From:");
		fromRange = new Text(toolsComposite, SWT.LEFT);
		fromRange.setLayoutData(newFillGridData());

		new Label(toolsComposite, SWT.LEFT).setText("To:");
		toRange = new Text(toolsComposite, SWT.LEFT);
		toRange.setLayoutData(newFillGridData());

		Button randomizeWeights = new Button(toolsComposite, SWT.PUSH);
		randomizeWeights.setText("Randomize Weights");
		randomizeWeights.setLayoutData(newFillGridData(2));
		randomizeWeights.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				randomizeWeights();
			}
		});

		randomizeBiases = new Button(toolsComposite, SWT.PUSH);
		randomizeBiases.setText("Randomize Biases");
		randomizeBiases.setLayoutData(newFillGridData(2));
		randomizeBiases.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				randomizeBiases();
			}
		});

		Button computeButton = new Button(toolsComposite, SWT.PUSH);
		computeButton.setText("Compute Neural Net");
		computeButton.setLayoutData(newFillGridData(4));
		computeButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				computeNeuralNet();
			}
		});
	}

	private void randomizeWeights() {
		try {
			double from = parseNumber(fromRange, "From");
			double to = parseNumber(toRange, "To");

			neuralNet.randoimzeWeights(from, to);
			repaintGraph();
		} catch (InputNumberFormatException e) {
			reportError("Cannot randomize Weights", e.getMessage(), e);
		}
	}

	private void randomizeBiases() {
		try {
			double from = parseNumber(fromRange, "From");
			double to = parseNumber(toRange, "To");

			neuralNet.randomizeBiasses(from, to);
			repaintGraph();
		} catch (InputNumberFormatException e) {
			reportError("Cannot randomize Biasses", e.getMessage(), e);
		}
	}

	private double parseNumber(Text fieldToParse, String propertiesName) throws InputNumberFormatException {
		try {
			return Double.parseDouble(fieldToParse.getText());
		} catch (NumberFormatException e) {
			throw new InputNumberFormatException(propertiesName, e);
		}
	}

	private int parseIntNumber(Text fieldToParse, String propertiesName) throws InputNumberFormatException {
		try {
			return Integer.parseInt(fieldToParse.getText());
		} catch (NumberFormatException e) {
			throw new InputNumberFormatException(propertiesName, e);
		}
	}

	private void createLayerSection() {
		Composite layerComposite = new Composite(composite, SWT.BORDER);

		GridLayout layout = new GridLayout();
		layout.numColumns = 2;
		layerComposite.setLayout(layout);

		new Label(layerComposite, SWT.BOLD | SWT.LEFT).setText("Layer:");
		layerName = new Text(layerComposite, SWT.LEFT | SWT.READ_ONLY);
		layerName.setLayoutData(newFillGridData());

		new Label(layerComposite, SWT.LEFT).setText("Activiation Function:");
		layerActivationFunction = new Combo(layerComposite, SWT.READ_ONLY);
		layerActivationFunction.setLayoutData(newFillGridData());
		layerActivationFunction.setItems(activationFunctionFactory.getAvailableFunctionsAsString());

		new Label(layerComposite, SWT.LEFT).setText("Activation Function Factor:");
		layerActivationFunctionFactor = new Text(layerComposite, SWT.LEFT);
		layerActivationFunction.setLayoutData(newFillGridData());

		saveDataToLayerButton = new Button(layerComposite, SWT.PUSH);
		saveDataToLayerButton.setText("Save");
		saveDataToLayerButton.setLayoutData(newFillGridData(2));
		saveDataToLayerButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try {
					saveDataToLayer();
				} catch (InputNumberFormatException e1) {
					reportError("Saving Error", e1.getMessage(), e1);
				}
			}
		});

	}

	private void saveDataToLayer() throws InputNumberFormatException {
		if (selectedLayer == null) {
			return;
		}

		boolean repaintGraph = false;

		if (!selectedLayer.isFirst()) {
			activationFunctionFactory.setFactor(layerActivationFunctionFactor.getText());
			selectedLayer.changeActivationFunction(activationFunctionFactory.getFunctionForSelectedIndex(layerActivationFunction
					.getSelectionIndex()));
			repaintGraph = true;
		}

		if (repaintGraph) {
			repaintGraph();
		}
	}

	private void fillLayerData() {
		if (selectedLayer == null) {
			return;
		}

		layerName.setText(selectedLayer.getName());

		if (!selectedLayer.isFirst()) {
			layerActivationFunction.setEnabled(true);
			layerActivationFunctionFactor.setEnabled(true);
			layerActivationFunction.setText(selectedLayer.getActivationFunction().getName());
			layerActivationFunctionFactor.setText(selectedLayer.getActivationFunction().getFactorAsString());
		} else {
			layerActivationFunction.setEnabled(false);
			layerActivationFunctionFactor.setEnabled(false);
			layerActivationFunction.setText("");
			layerActivationFunctionFactor.setText("");
		}
	}

	private void computeNeuralNet() {
		if (neuralNet != null) {
			neuralNet.compute();
		}

		repaintGraph();
	}

	private void createNeuronConnectionSection() {
		Composite edgeComposite = new Composite(composite, SWT.BORDER);

		GridLayout layout = new GridLayout();
		layout.numColumns = 2;
		edgeComposite.setLayout(layout);

		new Label(edgeComposite, SWT.BOLD | SWT.LEFT).setText("Connection:");
		edgeName = new Text(edgeComposite, SWT.LEFT | SWT.READ_ONLY);
		edgeName.setLayoutData(newFillGridData());

		new Label(edgeComposite, SWT.LEFT).setText("Weight:");
		edgeWeight = new Text(edgeComposite, SWT.LEFT);
		edgeWeight.setLayoutData(newFillGridData());

		Button saveDataToConnectionButton = new Button(edgeComposite, SWT.PUSH);
		saveDataToConnectionButton.setText("Save");
		saveDataToConnectionButton.setLayoutData(newFillGridData(2));
		saveDataToConnectionButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try {
					saveDataToNeronConnection();
				} catch (InputNumberFormatException e1) {
					reportError("Saving Error", e1.getMessage(), e1);
				}
			}
		});
	}

	private void fillEdgeData() {
		edgeName.setText("");
		edgeWeight.setText("");

		if (selectedNeuronConnection == null) {
			return;
		}

		edgeName.setText(selectedNeuronConnection.getName());
		edgeWeight.setText(selectedNeuronConnection.getWeightAsString());
	}

	private void saveDataToNeronConnection() throws InputNumberFormatException {
		if (selectedNeuronConnection == null) {
			return;
		}

		if (!edgeWeight.getText().equals("")) {
			try {
				double weight = Double.parseDouble(edgeWeight.getText());
				selectedNeuronConnection.setWegiht(weight);
				repaintGraph();
			} catch (NumberFormatException e) {
				throw new InputNumberFormatException("Weight", e);
			}
		}
	}

	private GridData newFillGridData(int columnsToSpan) {
		GridData gridData = new GridData();
		gridData.grabExcessHorizontalSpace = true;
		gridData.horizontalAlignment = SWT.FILL;
		gridData.horizontalSpan = columnsToSpan;
		return gridData;
	}

	private GridData newFillGridData() {
		return newFillGridData(0);
	}

	private void createNeuronSection() {
		Composite neuronComposite = new Composite(composite, SWT.BORDER);

		GridLayout layout = new GridLayout();
		layout.numColumns = 2;
		neuronComposite.setLayout(layout);

		new Label(neuronComposite, SWT.BOLD | SWT.LEFT).setText("Neuron:");
		vertexName = new Text(neuronComposite, SWT.LEFT | SWT.READ_ONLY);
		vertexName.setLayoutData(newFillGridData());

		new Label(neuronComposite, SWT.LEFT).setText("Value:");
		vertexValue = new Text(neuronComposite, SWT.LEFT);
		vertexValue.setLayoutData(newFillGridData());

		new Label(neuronComposite, SWT.LEFT).setText("Bias:");
		vertexBias = new Text(neuronComposite, SWT.LEFT);
		vertexBias.setLayoutData(newFillGridData());

		new Label(neuronComposite, SWT.LEFT).setText("Bias active:");
		biasActive = new Button(neuronComposite, SWT.CHECK);

		Button saveDataToNeuronButton = new Button(neuronComposite, SWT.PUSH);
		saveDataToNeuronButton.setText("Save");
		saveDataToNeuronButton.setLayoutData(newFillGridData(2));
		saveDataToNeuronButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try {
					saveDataToNeuron();
				} catch (InputNumberFormatException e1) {
					reportError("Saving Error", e1.getMessage(), e1);
				}
			}
		});
	}

	private void fillNeuronData() {
		vertexName.setText("");
		vertexValue.setText("");
		vertexBias.setText("");

		if (selectedNeuron == null) {
			return;
		}

		vertexName.setText(selectedNeuron.getName());

		if (selectedNeuron.isValueSetted()) {
			vertexValue.setText(selectedNeuron.getValue().toPlainString());
		} else {
			vertexValue.setText("");
		}
		vertexValue.setEnabled(selectedNeuron.isValueManualyChangeable());

		vertexBias.setEnabled(selectedNeuron.isBiasDefinable());
		if (selectedNeuron.isBiasDefinable()) {
			vertexBias.setText(selectedNeuron.getBiasAsString());
		}

		biasActive.setSelection(selectedNeuron.isBiasDefined());
	}

	private void saveDataToNeuron() throws InputNumberFormatException {
		if (selectedNeuron == null) {
			return;
		}

		boolean repaintGraph = false;

		if (!vertexBias.getText().equals("")) {
			try {
				double bias = Double.parseDouble(vertexBias.getText());
				selectedNeuron.setBiasAsDouble(bias);
				repaintGraph = true;
			} catch (NumberFormatException e) {
				throw new InputNumberFormatException("Bias", e);
			}
		}

		if (!vertexValue.getText().equals("")) {
			try {
				double vertexValueDouble = Double.parseDouble(vertexValue.getText());
				selectedNeuron.setValue(new BigDecimal(vertexValueDouble));
				repaintGraph();
			} catch (NumberFormatException e) {
				throw new InputNumberFormatException("Value", e);
			}
		}

		selectedNeuron.setBiasDefined(biasActive.getSelection());

		if (repaintGraph) {
			repaintGraph();
		}
	}

	private Combo createComboProperty(Composite composite, GridData layoutData, String Label, String[] items) {
		new Label(composite, SWT.LEFT).setText(Label);
		Combo combo = new Combo(composite, SWT.READ_ONLY);
		combo.setLayoutData(layoutData);
		combo.setItems(items);

		return combo;
	}

	private Text createTextProperty(Composite composite, GridData layoutData, String label) {
		new Label(composite, SWT.LEFT).setText(label);
		Text text = new Text(composite, SWT.LEFT);
		text.setLayoutData(layoutData);
		return text;
	}

	private Button createPushButton(Composite composite, GridData layoutData, String label) {
		Button button = new Button(composite, SWT.PUSH);
		button.setText(label);
		button.setLayoutData(layoutData);
		return button;
	}

	private void repaintGraph() {
		Activator
				.getDefault()
				.getPreferenceStore()
				.firePropertyChangeEvent(Activator.RELOAD_NEURAL_NET_VISUALISATION, new Object(), new Object());
	}

	@Override
	public void setFocus() {
		composite.setFocus();
	}

}
