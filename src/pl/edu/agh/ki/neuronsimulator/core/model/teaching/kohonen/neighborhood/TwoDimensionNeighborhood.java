package pl.edu.agh.ki.neuronsimulator.core.model.teaching.kohonen.neighborhood;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import pl.edu.agh.ki.neuronsimulator.core.exceptions.InvalidNeigborhoodException;
import pl.edu.agh.ki.neuronsimulator.core.model.teaching.kohonen.KohonenNeuron;

public class TwoDimensionNeighborhood extends Neighborhood {
	private Map<KohonenNeuron, Coordinates> neuronToCoordinatesMap;

	@Override
	public void buildNeighborhood(Collection<KohonenNeuron> neurons) {
		neuronToCoordinatesMap = new HashMap<KohonenNeuron, Coordinates>(neurons.size());
		int gridSize = (int) Math.ceil(Math.sqrt(neurons.size()));

		Iterator<KohonenNeuron> neuronsIt = neurons.iterator();
		for (int x = 0; x < gridSize; x++) {
			for (int y = 0; y < gridSize; y++) {
				if (neuronsIt.hasNext()) {
					KohonenNeuron currentNeuron = neuronsIt.next();
					neuronToCoordinatesMap.put(currentNeuron, new Coordinates(x, y));
				}
			}
		}
	}

	private class Coordinates {
		private int x;
		private int y;

		public Coordinates(int x, int y) {
			this.x = x;
			this.y = y;
		}

	}

	@Override
	public int getRadius(KohonenNeuron fromNeuron, KohonenNeuron toNeuron) {
		if (fromNeuron.equals(toNeuron)) {
			return 0;
		}

		Coordinates fromCoordinates = neuronToCoordinatesMap.get(fromNeuron);
		Coordinates toCoordinates = neuronToCoordinatesMap.get(toNeuron);

		if (fromCoordinates == null || toCoordinates == null) {
			throw new InvalidNeigborhoodException("Neurons " + fromNeuron.toString() + " and " + toNeuron.toString()
					+ " are not neigborhoods.");
		}

		// calculates 2D square neigborhoods
		int xRadius = Math.abs(fromCoordinates.x - toCoordinates.x) + 1;
		int yRadius = Math.abs(fromCoordinates.y - toCoordinates.y) + 1;

		return xRadius > yRadius ? xRadius : yRadius;
	}
}
