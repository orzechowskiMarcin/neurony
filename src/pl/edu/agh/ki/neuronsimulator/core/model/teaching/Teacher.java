package pl.edu.agh.ki.neuronsimulator.core.model.teaching;

import java.util.List;

import pl.edu.agh.ki.neuronsimulator.core.exceptions.CannotTeachNetException;
import pl.edu.agh.ki.neuronsimulator.core.exceptions.InvalidInputException;

public abstract class Teacher {

	public void teach(List<Double[]> parsedInputData, LearnigSpeedDTO dto, int epochAmountValue) throws CannotTeachNetException {
		if (!isInputDataSizeCorrect(parsedInputData)) {
			throw new CannotTeachNetException("Input data files have diffrent size then naural net input layer");
		}

		double[][] inputValuesTab = new double[parsedInputData.size()][];
		int recordIndex = 0;
		for (Double[] record : parsedInputData) {
			inputValuesTab[recordIndex] = new double[record.length];
			for (int valueIndex = 0; valueIndex < record.length; valueIndex++) {
				inputValuesTab[recordIndex][valueIndex] = record[valueIndex];
			}
			recordIndex++;
		}

		try {
			teach(inputValuesTab, dto, epochAmountValue);
		} catch (InvalidInputException e) {
			throw new CannotTeachNetException(e);
		}
	}

	private void teach(double[][] inputValuesTab, LearnigSpeedDTO speedsDTO, int iterationsAmount) throws InvalidInputException {
		int inputValuesNumber = 0;
		for (int iterationNumber = 0; iterationNumber < iterationsAmount; iterationNumber++) {
			teach(inputValuesTab[inputValuesNumber], speedsDTO);

			inputValuesNumber = (inputValuesNumber + 1) % inputValuesTab.length;
		}
	}

	protected abstract void teach(double[] inputValuesTab, LearnigSpeedDTO speedsDTO) throws InvalidInputException;

	protected abstract boolean isInputDataSizeCorrect(List<Double[]> parsedInputData);
}
