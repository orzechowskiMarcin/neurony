package pl.edu.agh.ki.neuronsimulator.core.model;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.UUID;

import pl.edu.agh.ki.neuronsimulator.core.exceptions.InvalidInputException;
import pl.edu.agh.ki.neuronsimulator.core.model.activation.ActivationFunction;
import pl.edu.agh.ki.neuronsimulator.core.model.activation.implementation.LinearFunction;

public class Layer {

	private final Map<Integer, Neuron> neurons;
	private final int layerNumber;
	private ActivationFunction activationFunction = new LinearFunction();

	public Layer(int layerNumber) {
		this.layerNumber = layerNumber;
		neurons = new TreeMap<Integer, Neuron>();
	}

	public Collection<Neuron> getNeurons() {
		return neurons.values();
	}

	public int getLayerNumber() {
		return layerNumber;
	}

	public Set<NeuronConnection> getAllNeuronConnections() {
		Set<NeuronConnection> connections = new HashSet<>();
		for (Neuron neuron : getNeurons()) {
			connections.addAll(neuron.getAllConnections());
		}
		return connections;
	}

	/**
	 * 
	 * @return amount of neurons
	 */
	public int getNeuronSize() {
		return neurons.size();
	}

	public void addNeuron(Neuron neuron) {
		neurons.put(neuron.getNumberInLayer(), neuron);
	}

	public void compute(UUID currentComputationUUID) {
		for (Neuron neuron : neurons.values()) {
			neuron.compute(currentComputationUUID);
		}
	}

	public boolean isFirst() {
		return layerNumber == 0;
	}

	public void changeActivationFunction(ActivationFunction newFunction) {
		this.activationFunction = newFunction;
	}

	public ActivationFunction getActivationFunction() {
		return activationFunction;
	}

	public String getName() {
		return "L_" + layerNumber;
	}

	protected Map<Integer, Neuron> getNeronsMap() {
		return neurons;
	}

	@Override
	public int hashCode() {
		return layerNumber;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof Layer)) {
			return false;
		}
		Layer other = (Layer) obj;
		return this.layerNumber == other.layerNumber;
	}

	public void setValues(double[] inputValues) throws InvalidInputException {
		if (inputValues.length != getNeuronSize()) {
			throw new InvalidInputException("Input size do not match input neurons amount.");
		}

		int i = 0;
		for (Neuron neuron : getNeurons()) {
			neuron.setValue(inputValues[i++]);
		}
	}

}
