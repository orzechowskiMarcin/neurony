package pl.edu.agh.ki.neuronsimulator.core.model.activation.implementation;

import java.math.BigDecimal;

import pl.edu.agh.ki.neuronsimulator.core.model.activation.ActivationFunction;

public class LinearFunction extends ActivationFunction {
	public static final String NAME = "Linear Function";

	public LinearFunction(double factor) {
		super(factor);
	}

	public LinearFunction() {
		this(1.0);
	}

	@Override
	public BigDecimal compute(BigDecimal argument) {
		return argument.multiply(new BigDecimal(getFactor()));
	}

	@Override
	public double compute(double argument) {
		return compute(BigDecimal.valueOf(argument)).doubleValue();
	}

	@Override
	public double computeDerivative(double argument) {
		return getFactor();
	}

	@Override
	public String getName() {
		return NAME;
	}

	public static class Creator extends ActivationFunction.Creator {

		@Override
		public String getName() {
			return NAME;
		}

		@Override
		public ActivationFunction newInstance(double factor) {
			return new LinearFunction(factor);
		}
	}

}
