package pl.edu.agh.ki.neuronsimulator.core.model.teaching.kohonen;

import pl.edu.agh.ki.neuronsimulator.core.model.Coordinates;
import pl.edu.agh.ki.neuronsimulator.core.model.Neuron;
import pl.edu.agh.ki.neuronsimulator.core.model.NeuronConnection;

public class KohonenNeuron {

	private Neuron neuron;

	public KohonenNeuron(Neuron neuron) {
		this.neuron = neuron;
	}

	public Neuron getNeuron() {
		return neuron;
	}

	public Coordinates calculateCoordinates() {
		Coordinates coordinates = new Coordinates(neuron.getInConnections().size());
		for (NeuronConnection conn : neuron.getInConnections()) {
			coordinates.setCoordinates(conn.getInputDimmensionNumber(), conn.getWeightAsDouble());
		}
		return coordinates;
	}

	public double calculateEuclidianDisctance(Coordinates fromCoordinate) {
		return calculateCoordinates().calculateEuclidianDistance(fromCoordinate);
	}

	public void teach(Coordinates inputCoordinates, double neigborhoodModification, double learningSpeed) {
		for (NeuronConnection conn : neuron.getInConnections()) {
			double currentWeight = conn.getWeightAsDouble();
			double inputValue = inputCoordinates.getValue(conn.getInputDimmensionNumber());

			double weightDelta = learningSpeed * neigborhoodModification * (inputValue - currentWeight);
			double newWeight = currentWeight + weightDelta;
			conn.setWegiht(newWeight);
		}
	}

}
