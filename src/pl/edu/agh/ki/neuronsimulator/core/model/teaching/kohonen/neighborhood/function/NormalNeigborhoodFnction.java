package pl.edu.agh.ki.neuronsimulator.core.model.teaching.kohonen.neighborhood.function;


public class NormalNeigborhoodFnction implements NeighborhoodFunction {

	@Override
	public double compute(int radius) {
		if (radius == 0) {
			return 1.0;
		}

		return 1.0 / radius;
	}
}
