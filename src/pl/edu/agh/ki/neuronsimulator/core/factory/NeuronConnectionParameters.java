package pl.edu.agh.ki.neuronsimulator.core.factory;

public class NeuronConnectionParameters {

	private double weight;

	public double getWeight() {
		return weight;
	}

	public NeuronConnectionParameters setWeight(double weight) {
		this.weight = weight;
		return this;
	}

	public NeuronConnectionParameters setWeight(String weight) {
		return setWeight(Double.parseDouble(weight));
	}
}
