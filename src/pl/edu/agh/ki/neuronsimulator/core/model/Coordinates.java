package pl.edu.agh.ki.neuronsimulator.core.model;

public class Coordinates {
	private double[] coordinates;

	public Coordinates(int amountOfDimensions) {
		coordinates = new double[amountOfDimensions];
	}

	public Coordinates(double[] coordinates) {
		this.coordinates = coordinates;
	}

	public void setCoordinates(int dimension, double coordinate) {
		coordinates[dimension] = coordinate;
	}

	public double calculateEuclidianDistance(Coordinates fromCoordinates) {
		double result = 0.0;
		for (int i = 0; i < coordinates.length; i++) {
			double diff = fromCoordinates.coordinates[i] - coordinates[i];
			result += Math.pow(diff, 2);
		}
		return Math.sqrt(result);
	}

	public double getValue(int inputDimmensionNumber) {
		return coordinates[inputDimmensionNumber];
	}

	public int size() {
		return coordinates.length;
	}
}
