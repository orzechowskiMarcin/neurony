package pl.edu.agh.ki.neuronsimulator.core.exceptions;

public class InvalidInputException extends Exception {

	private static final long serialVersionUID = 1L;

	public InvalidInputException(String message) {
		super(message);
	}

}
