package pl.edu.agh.ki.neuronsimulator.core.parser;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import pl.edu.agh.ki.neuronsimulator.core.exceptions.CannotTeachNetException;

public class InputDataFileParser {

	public static List<Double[]> parse(String fileName) throws CannotTeachNetException {
		Reader reader = null;
		try {
			reader = new FileReader(fileName);

			Iterable<CSVRecord> parser = CSVFormat
					.newBuilder()
					.withIgnoreEmptyLines(true)
					.withQuoteChar('"')
					.withIgnoreSurroundingSpaces(true)
					.withRecordSeparator(',')
					.parse(reader);

			List<List<Double>> parsedData = new ArrayList<>();
			for (CSVRecord record : parser) {
				List<Double> recordData = new ArrayList<>();
				for (String entry : record) {
					Double entryAsDouble = new Double(entry);
					recordData.add(entryAsDouble);
				}

				parsedData.add(recordData);
			}

			return convertToTabAndVerify(parsedData);
		} catch (IOException e) {
			throw new CannotTeachNetException("Error during parsing file with input data.", e);
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
				}
			}
		}
	}

	private static List<Double[]> convertToTabAndVerify(List<List<Double>> fileData) throws CannotTeachNetException {
		List<Double[]> convertedData = new ArrayList<>();

		Iterator<List<Double>> dataIt = fileData.iterator();
		if (!dataIt.hasNext()) {
			throw new CannotTeachNetException("Input data file cannot be epmty.");
		}
		int recordSize = dataIt.next().size();

		for (List<Double> record : fileData) {
			if (record.size() != recordSize) {
				throw new CannotTeachNetException("File racords must have the same size.");
			}

			Double[] recordAsTab = new Double[record.size()];
			int i = 0;
			for (Double entry : record) {
				recordAsTab[i++] = entry;
			}

			convertedData.add(recordAsTab);
		}

		return convertedData;
	}
}
