package pl.edu.agh.ki.neuronsimulator.core.parser;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import pl.edu.agh.ki.neuronsimulator.core.exceptions.NeuralNetSchemaException;
import pl.edu.agh.ki.neuronsimulator.core.factory.AbstractNeuralNetFactory;
import pl.edu.agh.ki.neuronsimulator.core.factory.FastForwardNeuralNetFactory;
import pl.edu.agh.ki.neuronsimulator.core.factory.NeuronConnectionDescription;
import pl.edu.agh.ki.neuronsimulator.core.factory.NeuronConnectionParameters;
import pl.edu.agh.ki.neuronsimulator.core.factory.NeuronDescription;
import pl.edu.agh.ki.neuronsimulator.core.factory.NeuronParameters;
import pl.edu.agh.ki.neuronsimulator.core.model.NeuralNet;

public class NeuronFileParser {
	//@formatter:off
	private static final String CONNECTION_DATA = "conn";
	private static final String NEURON_DATA = "neuron";
	private static final String DATA_TYPE_DISCRIMINATOR = "dataTypeDiscriminator";
	private static final String BEGIN_NEURON_LAYER_OR_NEURON_LAYER = "beginNeuronLayer";
	private static final String BEGIN_NEURON_NUMBER_OR_NEURON_NUMBER = "beginNeuronNumber";
	private static final String END_NEURON_LAYER_OR_BIAS = "endNeuronLayer";
	private static final String END_NEURON_NUMBER = "endNeuronNumber";
	private static final String WEIGHT = "weight";
	private static final String DEFAULT_WEIGHT = "0.5";
	private static final String[] FILE_FORMAT = new String[] {
			DATA_TYPE_DISCRIMINATOR,
			BEGIN_NEURON_LAYER_OR_NEURON_LAYER, 
			BEGIN_NEURON_NUMBER_OR_NEURON_NUMBER, 
			END_NEURON_LAYER_OR_BIAS,
			END_NEURON_NUMBER, 
			WEIGHT};
	//@formatter:on

	public static NeuralNet parse(String fileName, AbstractNeuralNetFactory neuralNetFactory) throws IOException, NeuralNetSchemaException {
		Reader reader = null;
		try {
			reader = new FileReader(fileName);

			Iterable<CSVRecord> parser = CSVFormat
					.newBuilder()
					.withIgnoreEmptyLines(true)
					.withQuoteChar('"')
					.withIgnoreSurroundingSpaces(true)
					.withRecordSeparator(',')
					.withHeader(FILE_FORMAT)
					.parse(reader);

			boolean useSimpleMode = false;
			List<String> neuronsInLayer = new ArrayList<>();

			for (CSVRecord record : parser) {
				if (record.get(DATA_TYPE_DISCRIMINATOR).equals(CONNECTION_DATA)) {
					NeuronConnectionDescription connDesc = new NeuronConnectionDescription();

					connDesc.setBeginNeuronLayer(record.get(BEGIN_NEURON_LAYER_OR_NEURON_LAYER))
							.setBeginNeuronNumber(record.get(BEGIN_NEURON_NUMBER_OR_NEURON_NUMBER))
							.setEndNeuronLayer(record.get(END_NEURON_LAYER_OR_BIAS))
							.setEndNeuronNumber(record.get(END_NEURON_NUMBER));

					NeuronConnectionParameters connParam = new NeuronConnectionParameters();
					connParam.setWeight(record.get(WEIGHT));

					neuralNetFactory.addConnectionDescription(connDesc, connParam);
				} else if (record.get(DATA_TYPE_DISCRIMINATOR).equals(NEURON_DATA)) {
					NeuronDescription neuronDesc = new NeuronDescription();

					neuronDesc.setNeuronLayer(record.get(BEGIN_NEURON_LAYER_OR_NEURON_LAYER)).setNeuronNumber(
							record.get(BEGIN_NEURON_NUMBER_OR_NEURON_NUMBER));

					NeuronParameters neuronParam = new NeuronParameters();
					neuronParam.setBias(record.get(END_NEURON_LAYER_OR_BIAS));

					neuralNetFactory.addNeuronDescription(neuronDesc, neuronParam);
				} else {
					useSimpleMode = true;
					neuronsInLayer.add(record.get(DATA_TYPE_DISCRIMINATOR));
				}
			}

			if (useSimpleMode) {
				neuralNetFactory = createFactoryWithDescriptions(neuronsInLayer);
			}

			return neuralNetFactory.buildNeuralNet();
		} finally {
			if (reader != null) {
				reader.close();
			}
		}
	}

	private static AbstractNeuralNetFactory createFactoryWithDescriptions(List<String> neuronsInLayer) {
		AbstractNeuralNetFactory factory = new FastForwardNeuralNetFactory();

		int layerNumber = 0;

		List<Integer> neuronsInLayerAsInt = convertToListOfInts(neuronsInLayer);
		Iterator<Integer> it = neuronsInLayerAsInt.iterator();
		int maxNeuronInFirstLayer = it.next();
		while (it.hasNext()) {
			int maxNeuronInSecondLayer = it.next();

			for (int neuronInFirstLayer = 0; neuronInFirstLayer < maxNeuronInFirstLayer; neuronInFirstLayer++) {
				for (int neuronInsSecondLayer = 0; neuronInsSecondLayer < maxNeuronInSecondLayer; neuronInsSecondLayer++) {
					NeuronConnectionDescription connDesc = new NeuronConnectionDescription();

					connDesc.setBeginNeuronLayer(layerNumber)
							.setBeginNeuronNumber(neuronInFirstLayer)
							.setEndNeuronLayer(layerNumber + 1)
							.setEndNeuronNumber(neuronInsSecondLayer);

					NeuronConnectionParameters connParam = new NeuronConnectionParameters();
					connParam.setWeight(DEFAULT_WEIGHT);

					factory.addConnectionDescription(connDesc, connParam);
				}
			}

			maxNeuronInFirstLayer = maxNeuronInSecondLayer;
			layerNumber++;
		}
		return factory;
	}

	private static List<Integer> convertToListOfInts(List<String> neuronsInLayer) {
		List<Integer> result = new ArrayList<>(neuronsInLayer.size());
		for (String neuronsAmount : neuronsInLayer) {
			int parsed = Integer.parseInt(neuronsAmount);
			result.add(parsed);
		}
		return result;
	}
}
